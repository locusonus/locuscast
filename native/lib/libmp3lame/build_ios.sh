#!/bin/bash
#
# build LAME static library for IOS (i.e. libmp3lame.a)
# based on the Superbil script from https://github.com/Superbil/build-lame-for-iOS
#

set -e

# LAME version
LAME_VERSION=3.100

# IOS deployment version
MIN_IOS_VERSION="9.0"

# define directories
SOURCE_DIR="src"
TARGET_INC_DIR="include/lame"
TARGET_LIB_DIR="lib/ios"
BUILD_FOLDER="build"
BUILD_DIR="${SOURCE_DIR}/${BUILD_FOLDER}"

# set the default make, compiler and lipo from Xcode
MAKE=${MAKE-$(xcrun --find make)}
CC=${CC-$(xcrun --find gcc)}
LIPO=${LIPO-$(xcrun --find lipo)}


# download LAME if needed
if [ ! -d ${SOURCE_DIR} ]; then
    LAME_SOURCE_DIR="lame-${LAME_VERSION}"
    LAME_ARCHIVE="${LAME_SOURCE_DIR}.tar.gz"
    curl -L -O "https://downloads.sourceforge.net/project/lame/lame/${LAME_VERSION}/${LAME_ARCHIVE}"
    tar xzpf ${LAME_ARCHIVE}
    mv ${LAME_SOURCE_DIR} ${SOURCE_DIR}
    rm ${LAME_ARCHIVE}
fi

# make the output folders
mkdir -p ${BUILD_DIR}
mkdir -p ${TARGET_INC_DIR}
mkdir -p ${TARGET_LIB_DIR}

build_lame()
{
    cd ${SOURCE_DIR}

    if [ -f "Makefile" ];then
        ${MAKE} distclean
    fi

    # SDK must lower case
    _SDK=$(echo ${SDK} | tr '[:upper:]' '[:lower:]')
    SDK_ROOT=$(xcrun --sdk ${_SDK} --show-sdk-path)

    # C compiler flags
    # gcc in xcode is clang
    # Ref: http://clang.llvm.org/docs/CommandGuide/clang.html
    CFLAGS="-arch ${PLATFORM} -pipe -std=c99 ${BITCODE} -isysroot ${SDK_ROOT} -miphoneos-version-min=${MIN_IOS_VERSION} ${MARCH}"

    # GNU Autoconf
    ./configure \
        CFLAGS="${CFLAGS}" \
        --host="${HOST}-apple-darwin" \
        --enable-static \
        --disable-shared \
        --disable-decoder \
        --disable-frontend \
        --disable-debug \
        --disable-dependency-tracking

    ${MAKE}

    cp "libmp3lame/.libs/libmp3lame.a" "${BUILD_FOLDER}/libmp3lame-${PLATFORM}.a"
    cd ../
}

merge_libs()
{
    _SDK=$(echo ${SDK} | tr '[:upper:]' '[:lower:]')
    
    # remove old libmp3lame.a or lipo will failed
    OUTPUT_LIB=${TARGET_LIB_DIR}/libmp3lame_${_SDK}.a
    if [ -f $OUTPUT_LIB ]; then
        rm $OUTPUT_LIB
    fi

    # merge all libraries to the final directory
    ${LIPO} -create ${BUILD_DIR}/* -output ${OUTPUT_LIB}
    
    # clean
    rm -rf ${BUILD_DIR}/*
}



# build device version
HOST="arm"
SDK="iPhoneOS"
BITCODE="-fembed-bitcode"

PLATFORM="arm64"
build_lame

PLATFORM="armv7"
build_lame

PLATFORM="armv7s"
build_lame

PLATFORM="arm64e"
build_lame

merge_libs


# build simulator version
HOST="i686"
SDK="iPhoneSimulator"
BITCODE="-fembed-bitcode-marker"
MARCH="-march=x86-64"

PLATFORM="x86_64"
build_lame

PLATFORM="i386"
build_lame

merge_libs


# copy headers
cp ${SOURCE_DIR}/include/*.h ${TARGET_INC_DIR}

# clean
cd ${SOURCE_DIR}
${MAKE} distclean
