/**
 *  OpenSLESDspSource.h, version 2.0
 *  Stéphane Cousot @ http://www.ubaa.net/
 *  LocusCast is released under GNU General Public License, version 3 or later
 *
 *
 *  This file is part of the LocusCast application.
 *  LocusCast is a mobile audio streaming app for Locus Sonus soundmap project.
 *  <http://locusonus.org/soundmap/>
 *
 *  Add OpenSLES sound system support for DarkIce.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team. DarkIce is © Copyright Tyrell Hungary and Rafael 
 *  Diniz and Ákos Maróy under the GNU General Public Licence version 3.
 *  <http://www.darkice.org>
 *
 *
 *  ------------------------------------------------------------------------------------------------
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  ------------------------------------------------------------------------------------------------
 */

#ifndef OPENSLES_SOURCE_H
#define OPENSLES_SOURCE_H

#ifndef __cplusplus
#error This is a C++ include file
#endif




#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Reporter.h"
#include "AudioSource.h"

#ifdef HAVE_OPENSLES_LIB
#include <SLES/OpenSLES.h>
#ifdef __ANDROID__
#include <SLES/OpenSLES_Android.h>
#define ANDROID_BUFFER 1
#endif
#else
#error configure for HAVE_OPENSLES
#endif

#ifdef __ANDROID__
#include <android/log.h>
#endif




#define OPENSLES_GAIN_MIN       -99.0f
#define OPENSLES_GAIN_MAX        12.0f



/**
 * Circular buffer object.
 */
typedef struct
{
    int     size;       // buffer size, determines how many data can be recorded before "over-running"
    int     read;       // index of oldest data (at which to read data)
    int     write;      // index at which to write new data
    short * data;       // audio data
}
OpenSLESCircularBuffer;

/**
 *  DSP metering information
 */
typedef struct
{
    float peaks[32]; // the peak level per channel
    float rms[32];   // the rms level per channel
}
OpenSLESDspMetering;




/**
 *  An audio input based on the OpenSLES sound system
 *
 *  @author  stéphane cousot @ http://www.ubaa.net/
 *  @version 1.0
 */
class OpenSLESDspSource : public AudioSource, public virtual Reporter
{
    private:

        /**
         *  Name of the capture PCM stream.
         */
        char *pcmName;

        /**
         *  OpenSLES engine interfaces.
         */
        SLObjectItf engine;
        SLEngineItf engineItf;
    
        /**
         *  OpenSLES recorder interfaces.
         */
        SLObjectItf recorder;
        SLRecordItf recorderItf;

        /**
         *  OpenSLES player interfaces (for input monitoring).
         */
        SLObjectItf player;
        SLPlayItf playerItf;
        SLObjectItf outputMix;
    
        /**
         *  The OpenSLES / Android audio buffer.
         */
        #ifdef ANDROID_BUFFER
        SLAndroidSimpleBufferQueueItf recorderBuf;
        SLAndroidSimpleBufferQueueItf playerBuf;
        #else
        SLBufferQueueItf recorderBuf;
        SLBufferQueueItf playerBuf;
        #endif
    
        /**
         * Audio block size
         * number of bytes captured per frame to the buffer per block
         */
        //int blockSize;
    
        /**
         * Stores number of bytes per frame. One frame
         * contains all samples per time instance.
         */
        int samples;

        /**
         * Input gain modifier
         */
        float gain;

        /**
         *  DSP metering info.
         */
        OpenSLESDspMetering meters;
    
        /**
         *  The number of packets transfered to the encoder.
         */
        int packets;
    
        /**
         *  Ring audio buffer
         */
        OpenSLESCircularBuffer buffer;
    
        /**
         *  Is the buffer is full?
         */
        bool isFull;
        
        /**
         *  Is the stream running?
         */
        bool running;

        /**
         * Enable input monitoring
         */
        bool monitoring;




    protected:

        /**
         *  Default constructor. Always throws an Exception.
         *
         *  @exception Exception
         */
        inline OpenSLESDspSource( void ) throw( Exception )
        {
            throw Exception( __FILE__, __LINE__);
        }

        /**
         *  Initialize the object
         *
         *  @param name the PCM to open.
         *  @exception Exception
         */
        void init( const char *name ) throw( Exception );

        /**
         *  De-iitialize the object
         *
         *  @exception Exception
         */
        void strip( void ) throw( Exception );

        /**
         *  Callback called by OpenSLES / Android when audio is available.
         */
        static void
        #ifdef ANDROID_BUFFER
        recorder_callback( SLAndroidSimpleBufferQueueItf bq, void *context );
        #else
        recorder_callback( SLBufferQueueItf bq, void *context );
        #endif

    
    public:

        /**
         *  Constructor.
         *
         *  @param name the PCM (e.g. "hwplug:0,0").
         *  @param sampleRate samples per second (e.g. 44100 for 44.1kHz).
         *  @param bitsPerSample bits per sample (e.g. 16 bits).
         *  @param channel number of channels of the audio source
         *                 (e.g. 1 for mono, 2 for stereo, etc.).
         *  @exception Exception
         */
        inline OpenSLESDspSource( const char *name, int sampleRate = 44100, int bitsPerSample = 16, int channel = 2 ) throw( Exception )
        : AudioSource( sampleRate, bitsPerSample, channel)
        {
            init( name);
        }

        /**
         *  Copy Constructor.
         *
         *  @param ds the object to copy.
         *  @exception Exception
         */
        inline OpenSLESDspSource( const OpenSLESDspSource &ds ) throw( Exception )
        : AudioSource( ds )
        {
            init( ds.pcmName);
        }

        /**
         *  Destructor.
         *
         *  @exception Exception
         */
        inline virtual ~OpenSLESDspSource( void ) throw( Exception )
        {
            strip();
        }

        /**
         *  Assignment operator.
         *
         *  @param ds the object to assign to this one.
         *  @return a reference to this object.
         *  @exception Exception
         */
        inline virtual OpenSLESDspSource & operator=( const OpenSLESDspSource &ds ) throw( Exception )
        {
            if ( this != &ds ) {
                strip();
                AudioSource::operator=( ds);
                init( ds.pcmName);
            }
            return *this;
        }

        /**
         *  Tell if the data from this source comes in big or little endian.
         *
         *  @return true if the source is big endian, false otherwise
         *
        virtual bool isBigEndian( void ) const throw();*/

        /**
         *  Open the OpenSLESDspSource.
         *  This does not put Alsa device into recording mode.
         *  To start getting samples, call either canRead() or read().
         *
         *  @return true if opening was successful, false otherwise
         *  @exception Exception
         *  
         *  @see #canRead
         *  @see #read
         */
        virtual bool open( void ) throw( Exception );

        /**
         *  Check if the OpenSLESDspSource is open.
         *
         *  @return true if the OpenSLESDspSource is open, false otherwise.
         */
        inline virtual bool isOpen ( void ) const throw()
        {
            return recorder!=NULL;
        }

        /**
         *  Check if the OpenSLESDspSource can be read from.
         *  Blocks until the specified time for data to be available.
         *  Puts the PCM into recording mode.
         *
         *  @param sec the maximum seconds to block.
         *  @param usec micro seconds to block after the full seconds.
         *  @return true if the OpenSLESDspSource is ready to be read from,
         *          false otherwise.
         *  @exception Exception
         */
        virtual bool canRead( unsigned int sec, unsigned int usec ) throw( Exception );

        /**
         *  Read from the OpenSLESDspSource.
         *  Puts the PCM into recording mode.
         *
         *  @param buf the buffer to read into.
         *  @param len the number of bytes to read into buf
         *  @return the number of bytes read (may be less than len).
         *  @exception Exception
         */
        virtual unsigned int read( void * buf, unsigned int len ) throw( Exception );

        /**
         *  Close the OpenSLESDspSource.
         *
         *  @exception Exception
         */
        virtual void close( void ) throw( Exception );

        /**
         *  Modify the input gain level linearly.
         *
         *  @param gain the required level.
         */
        virtual void  setInputGain( float gain );

        /**
         *  Enable or disable player output.
         *
         *  @param enable is headset is plugged
         */
        virtual void  setInputMonitoring( bool enable );

        /**
         *  Read the DSP meterering.
         *
         *  @param chan the channel target.
         *  @return the input level for the given channel in dB.
         */
        virtual float getInputLevel( int chan=0 );
    
        /**
         *  Return the number of packet transfered to the encoders.
         */
        int getTransferedPackets()
        {
            return packets;
        }
        
        /**
         *  Return the number of samples transfered to the encoders.
         */
        long getTransferedSamples()
        {
            return packets*samples;
        }
        
        /**
         *  Return the number of bytes transfered to the encoders.
         */
        long getTransferedBytes()
        {
            return getTransferedSamples() * 2 * getChannel();
        }

};

#endif  /* OPENSLES_SOURCE_H */

