/**
 * OpenSLESDspSource.cpp
 * This file is released under GNU General Public License, version 3 or later. See the header file
 * OpenSLESDspSource.h for more informations.
 */




#include <math.h>
#include "AudioSource.h"

// compile only if configured for OPENAL
#ifdef HAVE_OPENSLES_LIB

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Util.h"
#include "Exception.h"
#include "OpenSLESDspSource.h"





// -------------------------------------------------------------------------------------------------
//  File identity
// -------------------------------------------------------------------------------------------------
static const char fileid[] = "$Id: OpenSLESDspSource.cpp $";





// -------------------------------------------------------------------------------------------------
//  Initialize the object
// -------------------------------------------------------------------------------------------------

void OpenSLESDspSource::init( const char *name ) throw ( Exception )
{
    pcmName     = Util::strDup( name);

    engine      = NULL;
    engineItf   = NULL;
    recorder    = NULL;
    recorderItf = NULL;
    recorderBuf = NULL;
    player      = NULL;
    playerItf   = NULL;
    outputMix   = NULL;

    running     = false;
    monitoring  = false;
    isFull      = true;

    packets     = 0;
    samples     = 1024/getChannel(); // max: 4096 / 2 (sizeof(short)) / getChannel();
    gain        = 1.f;

    buffer.read  = 0;
    buffer.write = 0;
    buffer.size  = samples * sizeof(short) * 10;
    buffer.data  = new short[ buffer.size ];

    reportEvent(6, "ring buffer size: ", buffer.size);

}




// -------------------------------------------------------------------------------------------------
//  De-initialize the object
// -------------------------------------------------------------------------------------------------

void OpenSLESDspSource::strip( void ) throw ( Exception )
{
    if ( isOpen() ) {
        close();
    }

    for( int i=0; i<getChannel(); i++ ) {
        meters.peaks[i] = 0;
        meters.rms[i] = 0;
    }

    delete[] buffer.data;
    delete[] pcmName;
}





// -------------------------------------------------------------------------------------------------
//  Open the audio source
// -------------------------------------------------------------------------------------------------

bool OpenSLESDspSource::open( void ) throw ( Exception )
{
    if ( isOpen() ) {
        return false;
    }

    SLresult                    res;
    SLDataSource                source;
    SLDataSink                  sink;
    SLDataFormat_PCM            pcm;
    SLDataLocator_IODevice      mic;
    SLuint32 		            samplesrate;
    #ifndef ANDROID_BUFFER
    SLDataLocator_BufferQueue   buf;
    #else
    SLDataLocator_AndroidSimpleBufferQueue buf;
    #endif



    // check sample rate
    switch( getSampleRate() )
    {
        case 8000:
            samplesrate = SL_SAMPLINGRATE_8;
            break;
        case 11025:
            samplesrate = SL_SAMPLINGRATE_11_025;
            break;
        case 16000:
            samplesrate = SL_SAMPLINGRATE_16;
            break;
        case 22050:
            samplesrate = SL_SAMPLINGRATE_22_05;
            break;
        case 32000:
            samplesrate = SL_SAMPLINGRATE_32;
            break;
        case 44100:
            samplesrate = SL_SAMPLINGRATE_44_1;
            break;
        case 48000:
            samplesrate = SL_SAMPLINGRATE_48;
            break;

        default:
            throw Exception( __FILE__, __LINE__, "Invalid sample rate" );
    }



    // create engine
    res = slCreateEngine(&engine, 0, NULL, 0, NULL, NULL);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't create audio engine");

    // realize the engine in synchronous mode
    res = (*engine)->Realize(engine, SL_BOOLEAN_FALSE);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't realize audio engine");

    // get the engine interface
    res = (*engine)->GetInterface(engine, SL_IID_ENGINE, &engineItf);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "unable to get audio interface");




    // setup microphone device source
    // and buffer queue sink

    mic.locatorType     = SL_DATALOCATOR_IODEVICE;
    mic.deviceType      = SL_IODEVICE_AUDIOINPUT;
    mic.deviceID        = SL_DEFAULTDEVICEID_AUDIOINPUT;
    mic.device          = NULL;

    pcm.formatType      = SL_DATAFORMAT_PCM;
    pcm.numChannels     = getChannel();
    pcm.samplesPerSec   = samplesrate;
    pcm.bitsPerSample   = SL_PCMSAMPLEFORMAT_FIXED_16;  // check for getBitsPerSample(): 8, 16, 20, 24 or 32 ?
    pcm.containerSize   = 16;
    pcm.channelMask     = getChannel()==1 ? SL_SPEAKER_FRONT_CENTER : (SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT);
    pcm.endianness      = SL_BYTEORDER_LITTLEENDIAN;

    source.pLocator     = &mic;
    source.pFormat      = &pcm;
    #ifdef ANDROID_BUFFER
    buf.locatorType     = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
    #else
    buf.locatorType     = SL_DATALOCATOR_BUFFERQUEUE;
    #endif
    buf.numBuffers      = 2;

    sink.pLocator       = &buf;
    sink.pFormat        = &pcm;


    #ifdef ANDROID_BUFFER
    SLInterfaceID ids[1] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE};
    #else
    SLInterfaceID ids[1] = {SL_IID_BUFFERQUEUE};
    #endif
    SLboolean flags[1]   = {SL_BOOLEAN_TRUE};




    // create audio recorder
    res = (*engineItf)->CreateAudioRecorder(engineItf, &recorder, &source, &sink, 1, ids, flags);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't create audio recorder");

    // realizing the recorder in synchronous mode
    res = (*recorder)->Realize(recorder, SL_BOOLEAN_FALSE);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't realize audio recorder");

    // get the record interface
    res = (*recorder)->GetInterface(recorder, SL_IID_RECORD, &recorderItf);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "unable to get audio recorder interface");




    // create output mix
    res = (*engineItf)->CreateOutputMix(engineItf, &outputMix, 0, 0, 0);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't create output mix");

    res = (*outputMix)->Realize(outputMix, SL_BOOLEAN_FALSE);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't realize output mix");

    // configure player source
    SLDataSource psrc = {&buf, &pcm};

    // configure player sink
    SLDataLocator_OutputMix outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMix};
    SLDataSink psink = {&outmix, NULL};

    // create audio player (for input monitoring)
    res = (*engineItf)->CreateAudioPlayer(engineItf, &player, &psrc, &psink, 1, ids, flags);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't create audio player");

    // realizing the player in synchronous mode
    res = (*player)->Realize(player, SL_BOOLEAN_FALSE);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "can't realize audio player");

    // get the player interface
    res = (*player)->GetInterface(player, SL_IID_PLAY, &playerItf);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "unable to get audio player interface");

    // start playing
    res = (*playerItf)->SetPlayState(playerItf, SL_PLAYSTATE_PLAYING);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "unable to start player");






    // get the buffer queue interface
    #ifdef ANDROID_BUFFER
    res = (*recorder)->GetInterface(recorder, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &recorderBuf);
    res = (*recorder)->GetInterface(player, SL_IID_ANDROIDSIMPLEBUFFERQUEUE, &playerBuf);
    #else
    res = (*recorder)->GetInterface(recorder, SL_IID_BUFFERQUEUE, &recorderBuf);
    res = (*recorder)->GetInterface(player, SL_IID_BUFFERQUEUE, &playerBuf);
    #endif
    if ( res!=SL_RESULT_SUCCESS )  throw Exception( __FILE__, __LINE__, "unable to get buffer interface");

    // register callback on the buffer queue
    res = (*recorderBuf)->RegisterCallback(recorderBuf, recorder_callback, this);
    if ( res!=SL_RESULT_SUCCESS ) throw Exception( __FILE__, __LINE__, "unable to register buffer callback");




    /*SLmillisecond period;
    res = (*recorderBuf)->GetPositionUpdatePeriod( &period );
    __android_log_print( ANDROID_LOG_WARN, "LocusIce", "--- period: %i", period );

    res = (*recorderBuf)->SetPositionUpdatePeriod( recordItf, 1000 );
    if ( res!=SL_RESULT_SUCCESS ) {
        throw Exception( __FILE__, __LINE__, "unable to set buffer update period");
        return false;
    }*/






    // initialize metering data
    for( int i=0; i<getChannel(); i++ ) {
        meters.peaks[i] = 0;
        meters.rms[i] = 0;
    }

    return true;
}





// -------------------------------------------------------------------------------------------------
//  Check wether read() would return anything
// -------------------------------------------------------------------------------------------------

bool OpenSLESDspSource::canRead( unsigned int sec, unsigned int usec ) throw ( Exception )
{
    if ( !isOpen() ) {
        return false;
    }

    if ( !running ) {
        SLresult res = (*recorderItf)->SetRecordState(recorderItf,SL_RECORDSTATE_RECORDING);
        if ( res!=SL_RESULT_SUCCESS ) {
            throw Exception( __FILE__, __LINE__, "failed to record audio");
            return false;
        }

        running = true;
        packets = 0;

        // enqueue some empty buffers for the recorder
        (*recorderBuf)->Enqueue(recorderBuf, &buffer.data[0], samples*sizeof(short));
    }


    isFull = buffer.read==buffer.write;

    // or wait only few time ?
    if ( isFull ) {

        const unsigned int max_wait_time  = sec * 1000000;
        const unsigned int wait_increment = 10000;
        unsigned int       cur_wait       = 0;

        while(max_wait_time > cur_wait)
        {
            if ( !isOpen()  ) return false;
            if ( !isFull ) return true;

            cur_wait += wait_increment;
            usleep ( wait_increment );
        }
        return false;
    }

    usleep( usec );

    return true;
}





// -------------------------------------------------------------------------------------------------
//  Read from the audio source
// -------------------------------------------------------------------------------------------------

unsigned int OpenSLESDspSource::read( void *buf, unsigned int len ) throw ( Exception )
{

    if ( !isOpen() ) {
        return 0;
    }



    short* output = (short*)buf;
    int channels  = getChannel();

    double sum[channels];
    for( int i=0; i<channels; i++ ) sum[i] = 0;

    for (  int i=0, len=samples*channels; i<len; i+=channels ) {
        for ( int c=0; c<channels; c++ ) {
            int s = i+c;
            buffer.data[buffer.read+s] *= gain;
            output[s] = buffer.data[buffer.read+s];
            sum[c] += output[s]*output[s];
        }
    }

    // mic monitoring
    if ( monitoring &&  player!=NULL )
        (*playerBuf)->Enqueue(playerBuf, &buffer.data[buffer.read], samples*sizeof(short));
        //(*playerBuf)->Enqueue(playerBuf, &output[0], samples*sizeof(short));

    // metering
    for( int i=0; i<channels; i++ ) {
        meters.rms[i] = sqrt( sum[i]/samples );     // RMS amplitude in dB (with 1 normalized to 100 dB)
        meters.peaks[i] = meters.rms[i]*sqrt(2);    // peak
    }

    buffer.read += samples;
    buffer.read %= buffer.size;

    packets++;

    return samples*sizeof(short);

}




// -------------------------------------------------------------------------------------------------
//  Close the audio source
// -------------------------------------------------------------------------------------------------

void OpenSLESDspSource::close( void ) throw ( Exception )
{
    if ( !isOpen() ) {
        return;
    }

    if ( player!=NULL ) {
        (*player)->Destroy(player);
        player = NULL;
        playerItf = NULL;
        playerBuf = NULL;
    }

    if ( outputMix!=NULL ) {
        (*outputMix)->Destroy(outputMix);
        outputMix = NULL;
    }

    if ( recorder!=NULL ) {
        (*recorder)->Destroy(recorder);
        recorder = NULL;
        recorderItf = NULL;
        recorderBuf = NULL;
    }

    if ( engine!=NULL ) {
        (*engine)->Destroy(engine);
        engine = NULL;
        engineItf = NULL;
    }


    running = false;
}




// -------------------------------------------------------------------------------------------------
//  Control the audio source
// -------------------------------------------------------------------------------------------------

void OpenSLESDspSource::setInputGain( float _gain )
{
    gain = _gain;
}
void OpenSLESDspSource::setInputMonitoring( bool enable )
{
    monitoring = enable;
}

float OpenSLESDspSource::getInputLevel( int channel )
{
    if ( !isOpen() || channel>=getChannel() ) return OPENSLES_GAIN_MIN;

    float rms = meters.rms[channel];
    return rms<=0.0f ? OPENSLES_GAIN_MIN : 20.0f * log10f(rms) - 100.f;
}





// -------------------------------------------------------------------------------------------------
//  this callback handler is called every time a buffer finishes recording
// -------------------------------------------------------------------------------------------------

#ifdef ANDROID_BUFFER
void OpenSLESDspSource::recorder_callback( SLAndroidSimpleBufferQueueItf bq, void *context )
#else
void OpenSLESDspSource::recorder_callback( SLBufferQueueItf bq, void *context )
#endif
{
    OpenSLESDspSource *p = (OpenSLESDspSource*) context;
    p->isFull = false;

    if ( p->isOpen() ) {

        p->buffer.write += p->samples;
        p->buffer.write %= p->buffer.size;

        (*p->recorderBuf)->Enqueue(p->recorderBuf, &p->buffer.data[p->buffer.write], p->samples*sizeof(short));
    }
}

#endif // HAVE_OPENSLES_LIB

