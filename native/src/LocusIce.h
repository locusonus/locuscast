/**
 *  LocusIce.h, version 4.0
 *  Stéphane Cousot @ http://www.ubaa.net/
 *  LocusIce is released under GNU General Public License, version 3 or later
 *
 *  Audio upstream manager.
 *
 *  This file is part of the LocusCast application.
 *  LocusCast is a mobile audio streaming app for Locus Sonus soundmap project.
 *  <http://locusonus.org/soundmap/>
 *
 *
 *  ------------------------------------------------------------------------------------------------
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  ------------------------------------------------------------------------------------------------
 */

#pragma once



#include <stdarg.h>
#include <string.h>
#include <sstream>
#include <pthread.h>
#include <math.h>
#include <sys/utsname.h>

#include "vorbis/codec.h"
#include "vorbis/vorbisenc.h"
#include "lame/lame.h"

#include "json/json.h"

#include "darkice/config.h"
#include "darkice/DarkIce.h"
#include "darkice/TcpSocket.h"


#define SERVER_HOST "locusonus.org"



#ifdef __APPLE__
//#undef HAVE_OPENAL_LIB
#undef HAVE_FMOD_LIB
#define INPUT_GAIN_MIN  OPENAL_GAIN_MIN
#define INPUT_GAIN_MAX  OPENAL_GAIN_MAX
#endif

#ifdef __ANDROID__
#include <android/log.h>
#ifndef HAVE_OPENSLES_LIB
#define HAVE_OPENSLES_LIB 1
#endif
#define INPUT_GAIN_MIN  OPENSLES_GAIN_MIN
#define INPUT_GAIN_MAX  OPENSLES_GAIN_MAX
#endif


#ifdef HAVE_OPENAL_LIB
#undef DSP_DEVICE_NAME
#define DSP_DEVICE_NAME  "openal"
#endif
#ifdef HAVE_OPENSLES_LIB
#undef DSP_DEVICE_NAME
#define DSP_DEVICE_NAME  "opensl"
#endif
#ifdef HAVE_FMOD_LIB
#undef DSP_DEVICE_NAME
#define DSP_DEVICE_NAME  "fmod"
#endif

#if !defined( DSP_DEVICE_NAME )
#error Missing DSP audio device
#endif




using namespace std;



static const char * HTTP_RES_OK = "200";
static const char * HTTP_CON_ER = "HTTP CONNECTION ERROR";


// server configuration data
typedef struct
{
    int id;
    string name;
    string author;
    
    string buffersec;
    string channel;
    string samplerate;
    string bitrate;
    string quality;
    string format;
    
    string url;
    string mountpoint;
    string server;
    string pass;
    string port;
    
}
Data;




/**
 * Return the application bundle name.
 * MUST BE IMPLEMENTED IN OS LANGUAGE SCRIPT
 */
const char* getAppName();

/**
 * Return the application bundle version.
 * MUST BE IMPLEMENTED IN OS LANGUAGE SCRIPT
 */
const char* getAppVersion();

/**
 * Return system characteristics, dependencies version, etc.
 */
string getAppInformation();




// -------------------------------------------------------------------------------------------------
    #pragma mark configuration
// -------------------------------------------------------------------------------------------------

/**
 * Connect to the server to get configuration.
 *
 * @param name      the login name
 * @param pass      the login password
 * @return result   true if login was successful, false otherwise
 */
bool autoconfig( const char* name, const char* pass );

/**
 * Return informations about user configuration.
 * Note that this function return user settings, not the complete Darkice configuration.
 */
string getUserConfiguration();

/**
 * Return the current DarkIce configuration.
 */
string getDarkIceConfiguration();

/**
 * Return the configuration URL
 */
const char * getUrl();

/**
 * Return the configuration server address
 */
const char * getServer();

/**
 * Return the configuration server port
 */
int getPort();

/**
 * Return the configuration mountpoint
 */
const char * getMountpoint();

/**
 * Return the configuration audio encoding format
 */
const char * getFormat();

/**
 * Return the configuration stream name
 */
const char * getName();

/**
 * Set or get the configuration audio input channels
 * 1 for MONO, 2 for STEREO
 */
void setChannel( int input );
int  getChannel();

/**
 * Set or get the configuration audio quality
 */
void  setQuality( float value );
float getQuality();

/**
 * Set or get the configuration audio sample rate
 */
void setSampleRate( int value );
int  getSampleRate();

/**
 * Set or get the configuration buffer size in bytes
 */
void setBufferSize( int bytes );
int  getBufferSize();

/**
 * Set or get the configuration buffer duration in seconds
 */
void   setBufferDuration( double sec );
double getBufferDuration();

/**
 * Return the configuration audio bit rate
 */
int getBitRate();

/**
 * Enable or disable input monitoring
 */
void setInputMonitoring( bool enable );




// -------------------------------------------------------------------------------------------------
    #pragma mark stream
// -------------------------------------------------------------------------------------------------

/**
 * Return the stream ID
 */
int streamID();

/**
 * Init stream with the current configuration
 */
bool streamInit();

/**
 * Start streaming
 */
bool streamStart();

/**
 * Stop streaming
 */
bool streamStop();

/**
 * Run audio capture, encode data
 * and stream to the server
 * MUST BE CALLED ON THREAD
 */
void * streamRun( void* data );

/**
 * Send audio data to server.
 *
 * @param buffer    the data to be sent
 * @param samples   the size of the buffer
 */
void streamWrite( short *buffer, int size );

/**
 * Return whether the stream is running.
 */
bool streamIsRunning();

/**
 * Return whether the audio encoder is running or not.
 */
bool streamIsEncoding();

/**
 * Return whether the stream is mounted on the server.
 */
bool streamIsMounted();

/**
 * Adjust the capture input gain.
 */
void streamSetInputGain( float gain );

/**
 * Return the capture input level per channel.
 */
float streamGetInputLevel( int channel=0 );

/**
 *  Return the number of packet transfered to the audio encoder.
 */
int streamGetTransferedPackets();

/**
 *  Return the number of samples transfered to the audio encoder.
 */
long streamGetTransferedSamples();

/**
 *  Return the number of bytes transfered to the encoders.
 */
long streamGetTransferedBytes();

/**
 *  Get the normalized usage of the internal buffer.
 *  0 meaning perfect in<->out process, and 1.0 critical status
 */
float streamGetInternalBufferStatus();




// -------------------------------------------------------------------------------------------------
    #pragma mark network
// -------------------------------------------------------------------------------------------------

/**
 * Returns URI content,
 * If full is set to false, returns the headers without the response body.
 *
 * @param host      the network location of the URI
 * @param path      the absolute path of the URI
 * @param port      [the network port , default 80]
 * @param full      [whether full content is requested or headers only]
 * @return content  the URI content or the full URI headers on success
 */
string GET( const char* host, const char* path, int port=80, bool full=true );

/**
 * Send POST request to the given URI
 *
 * @param host      the network location of the URI
 * @param path      the absolute path of the URI
 * @param req       the absolute path of the URI
 * @return result   true on success, false otherwise
 */
bool POST( const char* host, const char* path, const char* req="" );

/**
 * Returns the full headers
 *
 * @param host      the network location of the URI
 * @param path      the absolute path of the URI
 * @param req       the absolute path of the URI
 * @return header   the URI headers, no content
 */
string HEAD( const char* host, const char* path, int port=80 );

/**
 * Returns the HTTP response code
 *
 * @param response  the request response
 * @return code     the response code
 */
int httpResponseCode( string response );



// -------------------------------------------------------------------------------------------------
    #pragma mark location
// -------------------------------------------------------------------------------------------------

/**
 * Send location to the server
 *
 * @param lat       the lattitude location
 * @param lng       the longitude location
 * @return result   true on success, false otherwise
 */
bool sendLocation( double lat, double lng );




// -------------------------------------------------------------------------------------------------
    #pragma mark log
// -------------------------------------------------------------------------------------------------

/**
 * Log message: 
 * use log() for verbose messages
 * use error() for error messages
 */
void log( const char* format, ... );
void error( const char* format, ... );

/**
 * Return the last process error
 */
const char * getLastError();




// -------------------------------------------------------------------------------------------------
    #pragma mark util
// -------------------------------------------------------------------------------------------------

/**
 * Convert a value to a string
 */
template<class T>
string toString( const T& value );

