/**
 * LocusIce.cpp
 * This file is released under GNU General Public License, version 3 or later. See the header file
 * LocusIce.h for more informations.
 */

#include "LocusIce.h"



// Audio encoder
static Ref<DarkIce> _darkice;



pthread_t _thread;
pthread_attr_t attr;
pthread_mutex_t _mutex;
pthread_cond_t _cond;

bool _running = false;

Data _data;
string _last_error;




string getAppInformation()
{
    
    string info;
    
    char system[1024];
    struct utsname u_name; uname(&u_name);
    sprintf( system, "%s, %s Kernel Version %s", u_name.machine, u_name.sysname, u_name.release );
    
    info += "System information:\n" + string(system) + "\n\n";
    info += "DarkIce version:\n" + string(PACKAGE_VERSION)+"\n\n";
    info += "Ogg library version:\n1.3.4\n\n";
    info += "Vorbis library version:\n" + string(vorbis_version_string())+"\n\n";
    info += "LAME library version:\n" + string(get_lame_version())+"\n\n";
    info += "JsonCpp library version:\n" + string(JSONCPP_VERSION_STRING);
    
    return info;
}




// -------------------------------------------------------------------------------------------------
    #pragma mark configuration
// -------------------------------------------------------------------------------------------------

bool autoconfig( const char * name, const char * pass )
{
    
    char path[1024];
    const char * base = "/soundmap/admin/config/json/%s/%s";
    sprintf( path, base, name, pass );

    string content = GET( SERVER_HOST, path );
    int code = httpResponseCode(content);
    
    if ( content.compare(0,strlen(HTTP_CON_ER),HTTP_CON_ER)==0 ) {
        error( "LOGIN FAILED\nconnection error" );
        return false;
    }
    //if ( content.compare(9,strlen(HTTP_RES_OK),HTTP_RES_OK)!=0 ) {
    if ( code!=200 ) {
        error( "LOGIN FAILED\nunexpected response from server [%i]", code );
        return false;
    }
    
    

    // parse JSON
    Json::CharReaderBuilder builder;
    Json::Value json;
    JSONCPP_STRING err;
    
    string body = content.substr(content.find("\r\n\r\n")+4);
    const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
    if ( !reader->parse(body.c_str(), body.c_str() + body.length(), &json, &err) ) {
      error( "Malformed stream configuration", err.c_str() );
      return false;
    }
    
    
    if ( json["error"] ) {
        error( json["error"].asString().c_str() );
        return false;
    }
    
    _data.id         = json["id"] ? json["id"].asInt() : 0;
    _data.name       = json["name"].asString();
    _data.author     = json["author"].asString();
    _data.pass       = json["pass"].asString();
    _data.server     = json["server"].asString();
    _data.port       = json["port"].asString();
    _data.mountpoint = json["mountpoint"].asString();
    _data.url        = json["url"].asString();
    _data.format     = json["format"].asString();
    _data.channel    = json["channel"].asString();
    _data.samplerate = json["samplerate"].asString();
    _data.quality    = json["quality"].asString();
    _data.bitrate    = json["bitrate"].asString();
    _data.buffersec  = json["buffersec"] ? json["buffersec"].asString() : "2";

    
    log( "Logged as %s", name );
    return true;
    
}

string getUserConfiguration()
{
    string conf;
    
    conf += "audio encoding format:\n" + (_data.format=="vorbis"?"ogg":_data.format) + "\n\n";
    conf += "audio input channel:\n" + _data.channel + "\n\n";
    conf += "audio sample rate:\n" + _data.samplerate + "\n\n";
    conf += "audio quality:\n" + _data.quality + "\n\n";
    //conf += "audio bit rate\n" + _data.bitrate + "\n\n";
    conf += "server address:\n" + _data.server + "\n\n";
    conf += "server port:\n" + _data.port + "\n\n";
    conf += "stream mountpoint:\n" + _data.mountpoint + "\n\n";
    conf += "stream url:\n" + _data.url + "\n\n";
    conf += "stream name:\n" + _data.name + "\n\n";
    conf += "stream author:\n" + _data.author;
    
    return conf;
}

string getDarkIceConfiguration()
{
    string conf;
    
    conf += "[general]\n";
    conf += "duration = 0\n";
    conf += "bufferSecs = " + _data.buffersec + "\n";
    conf += "reconnect = yes\n";
    conf += "realtime = no\n";
    conf += "[input]\n";
    conf += "device = " + string(DSP_DEVICE_NAME) + "\n";
    conf += "sampleRate = " + _data.samplerate + "\n";
    conf += "bitsPerSample = 16\n";
    conf += "channel = " + _data.channel + "\n";
    conf += "[icecast2-0]\n";
    conf += "bitrateMode = vbr\n";
    conf += "quality = " + _data.quality + "\n";
    conf += "format = " + _data.format + "\n";
    //conf += "bitrate = " + _data.bitrate + "\n";
    conf += "server = " + _data.server + "\n";
    conf += "port = " + _data.port + "\n";
    conf += "password = " + _data.pass + "\n";
    conf += "mountPoint = " + _data.mountpoint + "\n";
    conf += "name = " + _data.name + "\n";
    conf += "description = Live mobile stream by " + _data.author + " - " + getAppName() + " " + getAppVersion() + "\n";
    conf += "url = " + _data.url + "\n";
    conf += "genre = Live audio stream\n";
    conf += "public = yes\n";
    
    return conf;
}

const char * getServer()
{
    return _data.server.c_str();
}

int getPort()
{
    return atoi( _data.port.c_str() );
}

const char * getMountpoint()
{
    return _data.mountpoint.c_str();
}

const char * getUrl()
{
    return _data.url.c_str();
}

const char * getFormat()
{
    return _data.format.c_str();
}

const char * getName()
{
    return _data.name.c_str();
}

void setChannel( int value )
{
    _data.channel = toString(value);
}

int getChannel()
{
    return atoi( _data.channel.c_str() );
}

void setQuality( float value )
{
    _data.quality = toString(value);
}

float getQuality()
{
    return atof( _data.quality.c_str() );
}

void setSampleRate( int value )
{
    _data.samplerate = toString(value);
}

int getSampleRate()
{
    return atoi( _data.samplerate.c_str() );
}

void setBufferSize( int bytes )
{
    setBufferDuration( (double)(64*bytes)/getSampleRate() );
}

int getBufferSize()
{
    return roundf((getBufferDuration()/64)*getSampleRate());
}

void setBufferDuration( double sec )
{
    _data.buffersec = toString(sec);
}

double getBufferDuration()
{
    return atof( _data.buffersec.c_str() );
}

int getBitRate()
{
    return atoi( _data.bitrate.c_str() );
}

void setInputMonitoring( bool enable )
{
    if ( !_darkice.get() || !_darkice->getDsp() ) return;

    #ifdef HAVE_FMOD_LIB
    ((FMODDspSource*)_darkice->getDsp())->setInputMonitoring(enable);
    #endif
    #ifdef HAVE_OPENAL_LIB
    ((OpenALDspSource*)_darkice->getDsp())->setInputMonitoring(enable);
    #else
    #ifdef HAVE_OPENSLES_LIB
    return ((OpenSLESDspSource*)_darkice->getDsp())->setInputMonitoring(enable);
    #else
    #error Missing DSP function
    #endif
    #endif
}



// -------------------------------------------------------------------------------------------------
    #pragma mark stream
// -------------------------------------------------------------------------------------------------


int streamID()
{
    return _data.id;
}

bool streamInit()
{
    const char * vorbis = vorbis_version_string();
    
    log( "init stream ID: %i", _data.id );
    log( "using server: %s", _data.server.c_str() );
    log( "using port: %s", _data.port.c_str() );
    log( "using mountpoint: %s", _data.mountpoint.c_str() );
    log( "using audio encoding format: %s", _data.format.c_str() );
    log( "using audio input channel: %s", _data.channel.c_str() );
    log( "using audio sample rate: %s", _data.samplerate.c_str() );
    //log( "using audio bit rate: %s", _data.bitrate.c_str() );
    log( "using audio quality: %s", _data.quality.c_str() );
    log( "using audio buffer: %s", _data.buffersec.c_str() );
    
    if ( _data.format=="ogg" ) {
        _data.format = "vorbis";
        log( "using Vorbis encoding: %s", vorbis );
    }
    
    try
    {
        //
        //
        // REMOVE  REPORTER
        // DISABLE REPORTER
        //
        //
        #ifdef DEBUG
        Reporter::setReportVerbosity(6);
        Reporter::setReportOutputStream( std::cout );
        #endif
        
        string conf = getDarkIceConfiguration();
        istringstream iss(conf);
        Config config(iss);
        
        _darkice = new DarkIce(config);
        
    }
    catch ( Exception & e ) {
        error( "Audio encoder uninitialized\n%s", e.getDescription() );
        return false;
    }
    
    return true;
}

bool streamStart()
{
    if ( !_darkice.get() ) return false;
    
    
    pthread_mutex_init( &_mutex, NULL );
    pthread_cond_init ( &_cond, NULL );
    pthread_attr_init ( &attr );
    pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_JOINABLE );
    _running = !pthread_create( &_thread, &attr, streamRun, NULL );
    
    //pthread_cond_wait( &_cond, &_mutex );
    
    if ( !_running ) log( "Failed to start audio encoder" );
    return _running;
}

void * streamRun( void* data )
{
    log( "Start audio encoder" );
    pthread_mutex_lock( &_mutex );
    try {
        pthread_cond_signal( &_cond );
        _darkice->run();
        _last_error.clear();
    }
    catch ( Exception & e ) {
        error( "Audio encoder uninitialized\n%s", e.getDescription() );
    }
    pthread_mutex_unlock( &_mutex );
    log( "Stop audio encoder" );
    _running = false;
    
    pthread_exit(NULL);
    return NULL;
}

bool streamStop()
{
    if ( !_running || !streamIsEncoding() ) return false; // already stop or uninitialized
    
    _running = false;
    _darkice->getDsp()->close();
    
    pthread_join( _thread, NULL );
    pthread_attr_destroy(&attr);
    pthread_mutex_destroy(&_mutex);
    pthread_cond_destroy(&_cond);
    
    return true;
}

void streamWrite( short *buffer, int size )
{
    #ifdef HAVE_BUFFERED_SRC
    if ( DSP_DEVICE_NAME=="buffer" && _darkice->getDsp() )
        ((BufferedDspSource*)_darkice->getDsp())->write( buffer, size );
    #endif
}

bool streamIsRunning()
{
    return _running;
}

bool streamIsEncoding()
{
    if ( !_darkice.get() || !_darkice->getDsp() || !_running ) return false;
    return _darkice->getDsp()->isOpen();
}

bool streamIsMounted()
{
    string content = HEAD( getServer(), ("/"+_data.mountpoint).c_str(), getPort() );
    int code = httpResponseCode(content);
    //if ( content.compare(9,strlen(HTTP_RES_OK),HTTP_RES_OK)!=0 ) {
    if ( code!=200 ) {
        error( "Unmounted stream %s [%i]", getMountpoint(), code );
        return false;
    }
    
    return true;
}

void streamSetInputGain( float gain )
{
    if ( !_darkice.get() || !_darkice->getDsp() ) return;
    
    #ifdef HAVE_FMOD_LIB
    ((FMODDspSource*)_darkice->getDsp())->setInputGain(gain);
    #endif
    #ifdef HAVE_OPENAL_LIB
    ((OpenALDspSource*)_darkice->getDsp())->setInputGain(gain);
    #else
    #ifdef HAVE_OPENSLES_LIB
    return ((OpenSLESDspSource*)_darkice->getDsp())->setInputGain(gain);
    #else
    #error Missing DSP function
    #endif
    #endif
}

float streamGetInputLevel( int channel )
{
    if ( !_darkice.get() || !_darkice->getDsp() ) return INPUT_GAIN_MIN;
    
    #ifdef HAVE_FMOD_LIB
    return ((FMODDspSource*)_darkice->getDsp())->getInputLevel(channel);
    #endif
    #ifdef HAVE_OPENAL_LIB
    return ((OpenALDspSource*)_darkice->getDsp())->getInputLevel(channel);
    #else
    #ifdef HAVE_OPENSLES_LIB
    return ((OpenSLESDspSource*)_darkice->getDsp())->getInputLevel(channel);
    #else
    #error Missing DSP function
    #endif
    #endif
}

int streamGetTransferedPackets()
{
    if ( !_darkice.get() || !_darkice->getDsp() ) return 0;
    
    #ifdef HAVE_OPENAL_LIB
    return ((OpenALDspSource*)_darkice->getDsp())->getTransferedPackets();
    #endif
    #ifdef HAVE_OPENSLES_LIB
    return ((OpenSLESDspSource*)_darkice->getDsp())->getTransferedPackets();
    #endif
    
    return 0;
}

long streamGetTransferedSamples()
{
    if ( !_darkice.get() || !_darkice->getDsp() ) return 0;
    
    #ifdef HAVE_OPENAL_LIB
    return ((OpenALDspSource*)_darkice->getDsp())->getTransferedSamples();
    #endif
    #ifdef HAVE_OPENSLES_LIB
    return ((OpenSLESDspSource*)_darkice->getDsp())->getTransferedSamples();
    #endif
    
    return 0;
}

long streamGetTransferedBytes()
{
    if ( !_darkice.get() || !_darkice->getDsp() ) return 0;
    
    #ifdef HAVE_OPENAL_LIB
    return ((OpenALDspSource*)_darkice->getDsp())->getTransferedBytes();
    #endif
    #ifdef HAVE_OPENSLES_LIB
    return ((OpenSLESDspSource*)_darkice->getDsp())->getTransferedBytes();
    #endif
    
    return 0;
}

float streamGetInternalBufferStatus()
{
    if ( !_darkice.get() || _darkice->getEncoder()->getNumSinks()==0 ) return 0;
    
    BufferedSink *sink = (BufferedSink*)_darkice->getSink(0);
    return sink->getPeak() / sink->getBufferSize();
}




// -------------------------------------------------------------------------------------------------
    #pragma mark network
// -------------------------------------------------------------------------------------------------

string GET( const char * host, const char * path, int port, bool full )
{
    const int buflen = 1024;
    
    char hdr[buflen];
    char buf[buflen];
    char osi[buflen];
    
    string response, format;
    
    format  = "%s %s HTTP/1.1\r\n";
    format += "Host: %s\r\n";
    format += "User-Agent: %s/%s (http://%s/);%s\r\n";
    format += "\r\n";
    
    struct utsname u_name;
    if ( uname(&u_name)==-1 ) osi[0] = '\x00';
    else sprintf( osi, " %s/%s/%s", u_name.sysname, u_name.release, u_name.machine );
    
    sprintf( hdr, format.c_str(), full?"GET":"HEAD", path, host, getAppName(), getAppVersion(), SERVER_HOST, osi );

    try
    {
        TcpSocket socket(host,port);
        if ( socket.open() )
        {
            socket.write( hdr, (unsigned int)strlen(hdr) );
            socket.flush();
            
            int len  = socket.read( buf, buflen );
            buf[len] = '\x00'; response += buf;
            while( socket.canRead(0,0) && (len=socket.read(buf,buflen)) ) {
                buf[len] = '\x00';
                response += buf;
            }
            socket.close();
        }
    }
    catch ( Exception e ) {
        error( "%s: connection error to %s: %s", full?"GET":"HEAD", host, e.getDescription() );
        return HTTP_CON_ER;
    }
    
    return response;
}

string HEAD( const char * host, const char * path, int port )
{
    // FIXME: Icecast2 reacts to HTTP HEAD with 400 Bad Request
    // route the request via PHP, find another way or ask directly with a GET request ?
//    char hack[1024];
//    sprintf( hack, "/soundmap/checkurl?q=http://%s:%i%s", host, port, path );
//    return GET( SERVER_HOST, hack );
    return GET( host, path, port, true );
    // return GET( host, path, port, false );
}

bool POST( const char* host, const char* path, const char* req )
{
    const int buflen = 1024;
    
    char hdr[buflen];
    char buf[buflen];
    char osi[buflen];
    
    string response, format;
    
    format  = "POST %s HTTP/1.1\r\n";
    format += "Host: %s\r\n";
    format += "User-Agent: %s/%s (http://%s/);%s\r\n";
    format += "Content-Length: %i\r\n";
    format += "Content-Type: application/x-www-form-urlencoded\r\n";
    format += "\r\n%s";
    
    struct utsname u_name;
    if ( uname(&u_name)==-1 ) osi[0] = '\x00';
    else sprintf( osi, " %s/%s/%s", u_name.sysname, u_name.release, u_name.machine );
    
    sprintf( hdr, format.c_str(), path, host, getAppName(), getAppVersion(), SERVER_HOST, osi, strlen(req), req );
    
    try
    {
        TcpSocket socket(host,80);
        if ( socket.open() )
        {
            socket.write( hdr, (unsigned int)strlen(hdr) );
            socket.flush();
            
            int len  = socket.read( buf, buflen );
            buf[len] = '\x00'; response += buf;
            while( socket.canRead(0,0) && (len=socket.read(buf,buflen)) ) {
                buf[len] = '\x00';
                response += buf;
            }
            socket.close();
        }
    }
    catch ( Exception e ) {
        error( "POST: connection error to %s: %s", host, e.getDescription() );
        return false;
    }
    
    //if ( response.empty() || response.compare(9,strlen(HTTP_RES_OK),HTTP_RES_OK)!=0 ) {
    int code = httpResponseCode(response);
    if ( code!=200 ) {
        error( "POST error: unexpected response from server [%i]", code );
        return false;
    }
    
    response = response.substr(response.find("\r\n\r\n")+4);
    if ( !response.empty() ) error( response.c_str() );
    
    return response.empty();
}


int httpResponseCode( string response )
{
    if ( response.empty() || response.length()<9+strlen(HTTP_RES_OK) ) return -1;
    
    string code = response.substr(9,strlen(HTTP_RES_OK));
    return atoi( code.c_str() );
}




// -------------------------------------------------------------------------------------------------
    #pragma mark location
// -------------------------------------------------------------------------------------------------

bool sendLocation( double lat, double lng )
{
    char req[1024];
    sprintf( req, "/soundmap/admin/location/%i/%f/%f", _data.id, lat, lng );
    
    return POST( SERVER_HOST, req );
}




// -------------------------------------------------------------------------------------------------
    #pragma mark log
// -------------------------------------------------------------------------------------------------

void log( const char* format, ... )
{
    
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );
    
    
    #ifdef __ANDROID__
    __android_log_write( ANDROID_LOG_INFO, getAppName(), msg );
    #else
    cout << "[" << getAppName() << "::log] " << msg << endl;
    #endif
}

void error( const char* format, ... )
{
    
    char msg[1024];
    va_list args;
    
    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );
    
    
    #ifdef __ANDROID__
    __android_log_write( ANDROID_LOG_ERROR, getAppName(), msg );
    #else
    cerr << "[" << getAppName() << "::err] " << msg << endl;
    #endif
    
    _last_error = msg;
}

const char * getLastError()
{
    return _last_error.c_str();
};




// -------------------------------------------------------------------------------------------------
    #pragma mark util
// -------------------------------------------------------------------------------------------------

template<class T>
string toString( const T& value )
{
    ostringstream oss; oss << value;
    return oss.str();
}
