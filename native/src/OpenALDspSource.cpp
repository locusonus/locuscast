/**
 * OpenALDspSource.cpp
 * This file is released under GNU General Public License, version 3 or later. See the header file
 * OpenALDspSource.h for more informations.
 */




#include "AudioSource.h"

// compile only if configured for OPENAL
#ifdef HAVE_OPENAL_LIB

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "Util.h"
#include "Exception.h"
#include "OpenALDspSource.h"





// -------------------------------------------------------------------------------------------------
//  File identity
// -------------------------------------------------------------------------------------------------
static const char fileid[] = "$Id: OpenALDspSource.cpp $";





// -------------------------------------------------------------------------------------------------
//  Initialize the object
// -------------------------------------------------------------------------------------------------

void OpenALDspSource::init( const char *name ) throw ( Exception )
{
    pcmName    = Util::strDup( name);
    DSP        = NULL;
    samples    = 0;
    gain       = 1.f;
    running    = false;
    
    monitoring.enable  = false;
    monitoring.running = false;
    
    
    // liste devices
    const ALCchar* devs = alcGetString(NULL, ALC_CAPTURE_DEVICE_SPECIFIER);
    if (devs) {
        while( strlen(devs)>0 )
        {
            reportEvent(6, "OpenALDspSource: found device:", devs);
            devs += strlen(devs) + 1;
        }
    }
    
}




// -------------------------------------------------------------------------------------------------
//  De-initialize the object
// -------------------------------------------------------------------------------------------------

void OpenALDspSource::strip( void ) throw ( Exception )
{
    if ( isOpen() ) {
        close();
    }
    
    for( int i=0; i<getChannel(); i++ ) {
        meters.peaks[i] = 0;
        meters.rms[i] = 0;
    }
    
    delete[] pcmName;
}




// -------------------------------------------------------------------------------------------------
//  Open the audio source
// -------------------------------------------------------------------------------------------------

bool OpenALDspSource::open( void ) throw ( Exception )
{
    if ( isOpen() ) {
        return false;
    }
    
    if ( alcIsExtensionPresent(DSP, "ALC_EXT_CAPTURE")==AL_FALSE ) {
        throw Exception( __FILE__, __LINE__, "capture not available");
    }
    
    
    pthread_mutex_init( &mutex, NULL );
    pthread_cond_init ( &defer, NULL );
    
    
    // define audio format
    ALCenum format;
    switch( getBitsPerSample() ) {
        case 8:
            format = getChannel()==1 ? AL_FORMAT_MONO8 : AL_FORMAT_STEREO8;
            break;
        default:
            format = getChannel()==1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
    }
    
    
    // create recorder context
    DSP = alcCaptureOpenDevice( NULL, getSampleRate(), format, 4096/getChannel()/sizeof(short) );
    if ( !DSP ) {
        char errmsg[1024];
        sprintf( errmsg, "cannot open capture device [%i]", alcGetError(DSP) );
        throw Exception( __FILE__, __LINE__, errmsg);
    }
    
    
    // initialize metering
    packets = 0;
    for( int i=0; i<getChannel(); i++ ) {
        meters.peaks[i] = 0;
        meters.rms[i] = 0;
    }
    
    return true;
}




// -------------------------------------------------------------------------------------------------
//  Check wether read() would return anything
// -------------------------------------------------------------------------------------------------

bool OpenALDspSource::canRead( unsigned int sec, unsigned int usec ) throw ( Exception )
{
    if ( !isOpen() ) {
        return false;
    }

    pthread_mutex_lock( &mutex );
    
    if ( !running ) {
        alcCaptureStart(DSP);
        startInputMonitoring(); // monitor input async
        running = true;
    }
    
    
    ALCsizei nsamples = 0;
    alcGetIntegerv(DSP, ALC_CAPTURE_SAMPLES, 4, &nsamples);
    
    if ( nsamples==0 ) {
        
        const unsigned int max_wait_time  = sec * 1000000;
        const unsigned int wait_increment = 10000;
        unsigned int       cur_wait       = 0;
        
        // wait for data
        while( DSP && max_wait_time>cur_wait )
        {
            alcGetIntegerv(DSP, ALC_CAPTURE_SAMPLES, 4, &nsamples);
            if ( nsamples!=0 ) break;
           
            cur_wait += wait_increment;
            usleep ( wait_increment );
        }
    }
    
    
    samples = nsamples;
//    bufMonitoring.inp += samples;
//    if ( bufMonitoring.size ) bufMonitoring.inp %= bufMonitoring.size;
    
    usleep( usec );
    
    pthread_cond_signal ( &defer );
    pthread_mutex_unlock( &mutex );
    
    return samples!=0;
}




// -------------------------------------------------------------------------------------------------
//  Read from the audio source
// -------------------------------------------------------------------------------------------------

unsigned int OpenALDspSource::read( void *buf, unsigned int len ) throw ( Exception )
{
    
    if ( !isOpen() || !running ) {
        return 0;
    }
    
    
    short* output = (short*)buf;
    int channels  = getChannel();
    
    double sum[channels];
    for( int i=0; i<channels; i++ ) sum[i] = 0;
    
    if ( samples>=len ) samples = len/sizeof(short)/getChannel();
    alcCaptureSamples(DSP, output, samples);
    
    for( int i=0, len=samples*channels; i<len; i+=channels ) {
        for ( int c=0; c<channels; c++ ) {
            int s = i+c;
            output[s] *= gain;
            sum[c] += output[s]*output[s];
        }
    }
    
    // metering
    packets++;
    for( int i=0; i<channels; i++ ) {
        meters.rms[i] = sqrt( sum[i]/samples );     // RMS amplitude in dB (with 1 normalized to 100 dB)
        meters.peaks[i] = meters.rms[i]*sqrt(2);    // peak
    }
    
    
    return samples*sizeof(short)*channels;
}


// -------------------------------------------------------------------------------------------------
//  Close the audio source
// -------------------------------------------------------------------------------------------------

void OpenALDspSource::close( void ) throw ( Exception )
{
    if ( !isOpen() ) {
        return;
    }
    
    pthread_mutex_lock( &mutex );

    // FIX
    // !!! DO NOT CLOSE DEVICE NOW: alcGetIntegerv() work asynchrone twice (see above)
    // prefers wait the end of the canRead process.
    if ( running && samples!=0 ) pthread_cond_wait( &defer, &mutex );

    // stop input monitring and wait for thread
    stopInputMonitoring();
    while( monitoring.running );
    
    alcCaptureStop( DSP );
    alcCaptureCloseDevice( DSP );
    
    DSP = NULL;
    running = false;
    
    pthread_mutex_unlock( &mutex );
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&defer);

}




// -------------------------------------------------------------------------------------------------
//  Control the audio source
// -------------------------------------------------------------------------------------------------

void OpenALDspSource::setInputGain( float _gain )
{
    gain = _gain;
}

float OpenALDspSource::getInputLevel( int channel )
{
    if ( !isOpen() || channel>=getChannel() ) return OPENAL_GAIN_MIN;
    
    float rms = meters.rms[channel];
    return rms<=0.0f ? OPENAL_GAIN_MIN : 20.0f * log10f(rms) - 100.f;
}




// -------------------------------------------------------------------------------------------------
//  Monitor the audio source
// -------------------------------------------------------------------------------------------------

void OpenALDspSource::setInputMonitoring( bool enable )
{
    monitoring.enable = enable;
    
    if ( !monitoring.enable ) stopInputMonitoring();
    else startInputMonitoring();
}

void OpenALDspSource::startInputMonitoring()
{
    if ( alcIsExtensionPresent(DSP, "ALC_EXT_CAPTURE")==AL_FALSE ) return;
    if ( monitoring.enable && isOpen() && !monitoring.running )
        monitoring.running = !pthread_create( &monitoring.thread, NULL, &OpenALDspSource::RunInputMonitoring, this );
}

void OpenALDspSource::stopInputMonitoring()
{
    monitoring.running = false;
}

void OpenALDspSource::InputMonitoring()
{
    
    ALuint buffer;
    ALuint ringbuffer[2];
    ALuint source;
    ALint  processed;
    ALint  playing;
    
    
    
    int channels  = 2; // force stereo for headset
    int samplerate = getSampleRate();
    int datasize  = 4096*channels;
    short * data  = new short[ datasize ];
    
    
    
    // create contextes
    ALCdevice  *mic = alcCaptureOpenDevice( NULL, samplerate, AL_FORMAT_STEREO16, datasize/sizeof(short) );
    ALCdevice  *dev = alcOpenDevice( alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER) );
    ALCcontext *ctx = alcCreateContext(dev, NULL);
    
    // attempts to send error
    if ( mic==NULL || dev==NULL || !alcMakeContextCurrent(ctx) ) {
        throw Exception( __FILE__, __LINE__, "Unable to make direct input monitoring");
    }
    
    //create a source and a buffer
    alGenSources((ALuint)1, &source);
    alGenBuffers((ALuint)2, ringbuffer);
    
    // setup some initial silent data to play out of the source
    alBufferData(ringbuffer[0], AL_FORMAT_MONO16, data, sizeof(short)*channels, samplerate);
    alBufferData(ringbuffer[1], AL_FORMAT_MONO16, data, sizeof(short)*channels, samplerate);
    alSourceQueueBuffers(source, 2, ringbuffer);
    
    // start player and recorder
    alSourcePlay( source);
    alcCaptureStart( mic );
    
    
    while( monitoring.enable && monitoring.running ) {
        
        // ckeck if some data are required
        alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);
        if ( processed<=0 ) continue;
        
        ALCsizei samples = 0;
        alcGetIntegerv( mic, ALC_CAPTURE_SAMPLES, 4, &samples );
        if ( samples!=0 ) {
            
            // grab data
            alcCaptureSamples( mic, data, samples );
            for( int i=0; i<samples*channels; i+=2 ) {
                data[i+0] *= gain;
                data[i+1] *= gain;
            }
        
            // queue data
            alSourceUnqueueBuffers( source, 1, &buffer );
            alBufferData( buffer, AL_FORMAT_STEREO16, data, samples*sizeof(short)*channels, samplerate );
            alSourceQueueBuffers(source, 1, &buffer);
            
            // play if stopped
            alGetSourcei(source, AL_SOURCE_STATE, &playing);
            if ( playing!=AL_PLAYING ) alSourcePlay(source);
        }
        
        usleep(50000); // 50ms
    }
    
    monitoring.running = false;

    alSourceStop(source);
    alDeleteSources(1, &source);
    alDeleteBuffers(2, ringbuffer);
    alDeleteBuffers(1, &buffer);
    
    alcMakeContextCurrent(NULL);
    alcDestroyContext(ctx);
    alcCloseDevice(dev);

    alcCaptureStop( mic );
    alcCaptureCloseDevice( mic );
}

#endif // HAVE_OPENAL_LIB

