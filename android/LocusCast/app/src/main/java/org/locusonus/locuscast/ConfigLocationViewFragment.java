/**
 *	ConfigLocationViewFragment.java
 *	liveSHOUT
 *
 *
 *  liveSHOUT is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using liveSHOUT please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  liveSHOUT was designed at the Sonic Arts Research Centre <http://www.sarc.qub.ac.uk> in
 *  partnership with Locus Sonus <http://locusonus.org/> and was made possible through an Arts and
 *  Humanities Research Council research grant led by Dr Franziska Schroeder and Professor Pedro
 *  Rebelo at Queen’s University Belfast.
 *  liveSHOUT is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */
package org.locusonus.locuscast;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;



public class ConfigLocationViewFragment extends Fragment implements SeekBar.OnSeekBarChangeListener
{

    SeekBar loctime;
    SeekBar locdist;

    TextView locdistT;
    TextView locdistV;
    TextView loctimeT;
    TextView loctimeV;

    Activity context;


    ConfigLocationViewFragment(Activity activity) { context = activity; }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppData.loadLocationSettings();
        AppData.markUnchanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.config_location_layout, container, false);

        locdist  = (SeekBar)view.findViewById(R.id.locdist);
        locdistT = (TextView)view.findViewById(R.id.locdistT);
        locdistV = (TextView)view.findViewById(R.id.locdistV);
        loctime  = (SeekBar)view.findViewById(R.id.loctime);
        loctimeT = (TextView)view.findViewById(R.id.loctimeT);
        loctimeV = (TextView)view.findViewById(R.id.loctimeV);

        locdistV.setText(Integer.toString(AppData.locdist));
        loctimeV.setText(Integer.toString(AppData.loctime));

        loctime.setProgress(AppData.loctime);
        loctime.setOnSeekBarChangeListener(this);
        locdist.setProgress(AppData.locdist);
        locdist.setOnSeekBarChangeListener(this);

        return view;
    }



    // -- SLIDER -----------------------------------------------------------------------------------

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b)
    {
        switch (seekBar.getId()){
            case R.id.loctime:
                if ( progress/60==0 ) loctimeT.setText( String.format("%02d seconds", progress%60) );
                else loctimeT.setText( String.format("%02d minute %02d seconds", progress/60, progress%60) );
                loctimeV.setText(Integer.toString(progress));
                break;
            case R.id.locdist:
                if ( progress==0 )locdistT.setText("all movements");
                else if ( progress/1000==0 ) locdistT.setText(String.format("%d meters", progress));
                else locdistT.setText(String.format("%d kilometers", progress/1000));
                locdistV.setText(Integer.toString(progress));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {
        int value = seekBar.getProgress();

        switch (seekBar.getId()){
            case R.id.loctime:
                AppData.saveLocationTime(value);
                loctimeV.setText(Integer.toString(value));
                break;
            case R.id.locdist:
                AppData.saveLocationDistance(value);
                locdistV.setText(Integer.toString(value));
                break;
        }

        clearSeekBarMessage(seekBar);
    }

    private void clearSeekBarMessage(SeekBar seekBar)
    {
        switch (seekBar.getId()){
            case R.id.loctime:
                loctimeT.setText("in seconds");
                break;
            case R.id.locdist:
                locdistT.setText("in meters (set 0 to specify all movements)");
                break;
        }
    }
}
