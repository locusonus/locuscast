package org.locusonus.locuscast;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioDeviceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity implements
		ActivityCompat.OnRequestPermissionsResultCallback,
		LocusCast.OnPermissionListener
{

	StreamViewFragment streamView;
	ConfigViewFragment configView;
	ConfigAudioViewFragment audioView;
	ConfigLocationViewFragment locationView;

	View confView;
	View mainView;

	Fragment fragment;

	boolean isConfigured = false;
	ImageView loader;

	AudioDeviceInfo audioDeviceInfo;
	BroadcastReceiver receiver;




	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);

		// init
		initView();
		initNativeLib();

		AppData.register(this);
		LocusCast.register(this);

		streamView 	 = new StreamViewFragment(this);
		configView 	 = new ConfigViewFragment(this);
		audioView  	 = new ConfigAudioViewFragment(this);
		locationView = new ConfigLocationViewFragment(this);

		// register listener
		LocusCast.setOnPermissionListener(this);

		// check configuration
		startConfiguration();

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
	{
		LocusCast.onRequestPermissionsResult(requestCode,permissions,grantResults);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);

		boolean screenOn;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) screenOn = pm.isInteractive();
		else screenOn = pm.isScreenOn();

		if (screenOn) LocusCast.stopStream();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.unregisterReceiver(receiver);
	}

	@Override
	public void onLowMemory()
	{
		super.onLowMemory();

		LocusCast.stopStream();
		LocusCast.displayError("Memory warning.\nLocusCast has stopped the stream, Please reconnect.");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if ( fragment!=null ) fragment.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		//No call for super(). Bug on API Level > 11.
	}




	// -- CONFIGURATION ----------------------------------------------------------------------------

	void startConfiguration()
	{
		isConfigured = false;

		waitForConfiguration();
		runConfiguration();
	}

	void runConfiguration()
	{

		// ask for all permissions
		LocusCast.askForAllRequiredPermissions();

		// configure audio session
		if ( LocusCast.hasAudioPermission() ) initAudioSession();


		new Thread(new Runnable() {
			@Override
			public void run() {

				// sync configuration with server
				syncConfiguration();

				// view tabbar
				stopConfiguration();
			}
		}).start();
	}

	void stopConfiguration()
	{
		isConfigured = true;

		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
			loader.clearAnimation();
			loader.setAlpha(0);
			switchToMainView();
			}
		});
	}

	void waitForConfiguration()
	{
		loader = (ImageView) findViewById(R.id.logo);

		Animation animation = new AlphaAnimation(0,1);
		animation.setDuration(100);
		animation.setInterpolator(new LinearInterpolator());
		animation.setRepeatCount(Animation.INFINITE);
		animation.setRepeatMode(Animation.REVERSE);

		loader.startAnimation(animation);
	}

	void syncConfiguration()
	{
		AppData.loadLogin();
		if ( !AppData.isEmpty() ) {
			boolean isLogged = LocusCast.autoconfig(AppData.logname,AppData.logpass);
			if ( !isLogged ) LocusCast.displayError(LocusCast.getLastError());
		}
	}

	public void dumpConfiguration( View view )
	{
		try {
			PackageManager pm  = this.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(getPackageName(),0);
			ApplicationInfo ai = pm.getApplicationInfo(getPackageName(),0);

			String version = pi.versionName;
			String name = (String)pm.getApplicationLabel(ai);

			String conf = LocusCast.getUserConfiguration();
			String appinfo = LocusCast.getAppInformation();

			new AlertDialog.Builder(this)
					.setTitle("Configuration")
					.setMessage( name+" version\n"+version+"\n\n"+conf+"\n\n"+appinfo )
					.setNegativeButton(R.string.close, null)
					.setPositiveButton(R.string.copy, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							int api = android.os.Build.VERSION.SDK_INT;
							String conf = LocusCast.getUserConfiguration();
							if ( api>=android.os.Build.VERSION_CODES.HONEYCOMB ) {
								android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
								android.content.ClipData clip = android.content.ClipData.newPlainText("LocusCast configuration", conf);
								clipboard.setPrimaryClip(clip);
							}
							else {
								android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
								clipboard.setText( conf );
							}
						}
					})
					.show();
		}
		catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
	}



	// -- AUDIO ------------------------------------------------------------------------------------

	@Override
	public void OnGrantedAudioPermission(boolean granted)
	{
		if ( granted ) initAudioSession();
		else Log.i("[AUDIO SESSION]", "record permission : no" );
	}

	@Override
	public void OnGrantedLocationPermission(boolean granted) {}

	void initAudioSession()
	{
		int samplerate  = AppData.DEFAULT_SAMPLEREATE;
		int buffer      = 512;
		int outchannels = 2;
		int inchannels  = 1;


		android.media.AudioManager session = (android.media.AudioManager) this.getSystemService(this.AUDIO_SERVICE);

		Log.i("[AUDIO SESSION]", "record permission : yes" );


		// query the actual supported rate and max channels
		if (Build.VERSION.SDK_INT>=17) {
			String prate = session.getProperty(android.media.AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
			if ( prate!=null ) samplerate = Integer.parseInt(prate);
			String pchan = session.getProperty(android.media.AudioManager.EXTRA_MAX_CHANNEL_COUNT);
			if ( pchan!=null ) outchannels = Integer.parseInt(pchan);
		}
		Log.i( "[AUDIO SESSION]", "sample rate: "+samplerate+", number of output channels: "+outchannels );


		// get device input channel
		if (Build.VERSION.SDK_INT>=24) {
			AudioDeviceInfo[] inputs = session.getDevices(android.media.AudioManager.GET_DEVICES_INPUTS);
			if ( inputs.length>0 ) {
				int[] channels = inputs[0].getChannelCounts();
				inchannels = channels[channels.length - 1];
			}
		}
		updateNumberOfInputChannels( inchannels );


		// add audio routing listener
        /*if (Build.VERSION.SDK_INT>=28) {
            MediaRecorder recorder = new MediaRecorder();
            recorder.addOnRoutingChangedListener(this, null);
            audioDeviceInfo = recorder.getRoutedDevice();
        }*/


	}

	void updateNumberOfInputChannels( int channels )
	{
		AppData.updateDeviceInputChannel( channels );
		Log.i("[AUDIO SESSION]", "device input channel: "+AppData.devchannel );
	}

    /*@Override
    public void onRoutingChanged(AudioRouting routing)
    {
        if (Build.VERSION.SDK_INT>=28) {

            AudioDeviceInfo pinfo = audioDeviceInfo;
            audioDeviceInfo = routing.getRoutedDevice();

            int[] pchannels = pinfo.getChannelCounts();
            int[] channels = audioDeviceInfo.getChannelCounts();
            if ( channels.length!=0 ) updateNumberOfInputChannels( channels[channels.length-1] );

            Log.i(
                    "[AUDIO SESSION]",
                    "route has changed from "+(pchannels.length!=0?pchannels[pchannels.length-1]:"?")+"ch "+pinfo.getProductName()+
                    " to "+(channels.length!=0?channels[channels.length-1]:"?")+"ch "+audioDeviceInfo.getProductName()
            );
        }
    }*/



	// -- VIEW -------------------------------------------------------------------------------------

	void initView()
	{
		confView = (View) findViewById(R.id.confView);
		mainView = (View) findViewById(R.id.mainView);

		mainView.setVisibility(View.GONE);
	}

	void switchToMainView()
	{
		confView.animate().alpha(0.0f).setDuration(3000).setListener(new AnimatorListenerAdapter()
		{
			@Override
			public void onAnimationEnd(Animator animation) {
				super.onAnimationEnd(animation);
				confView.setVisibility(View.GONE);
				mainView.setVisibility(View.VISIBLE);
				switchToFragment( AppData.isEmpty() ? R.id.open_config_panel : R.id.open_stream_panel );
				LocusCast.hasLocationPermission();
			}
		}).start();
	}

	public void switchToView( View item )
	{
		switchToFragment(item.getId());
	}

	public void switchToStreamView()
	{
		switchToFragment(R.id.open_stream_panel);
	}

	public void switchToConfigView()
	{
		switchToFragment(R.id.open_config_panel);
	}

	void switchToFragment( int id )
	{
		fragment = null;

		switch (id) {
			case R.id.open_stream_panel:
				fragment = (Fragment)streamView;
				break;
			case R.id.open_config_panel:
				fragment = (Fragment)configView;
				break;
			case R.id.open_audio_panel:
				fragment = (Fragment)audioView;
				break;
			case R.id.open_location_panel:
				fragment = (Fragment)locationView;
				break;
		}

		if ( fragment!=null ) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace( R.id.container, fragment );
			ft.commit();
		}

	}



	// -- TWITTER ----------------------------------------------------------------------------------

	public void sendTweet( View view )
	{
		Intent intent;
		String message = "#locustream soundmap, ";

		try {
			intent = new Intent( Intent.ACTION_SEND );
			intent.setClassName( "com.ic_twitter.android","com.ic_twitter.android.PostActivity" );
			intent.putExtra( Intent.EXTRA_TEXT, message );
			startActivity( intent );
		}
		catch( Exception e ) {
			//displayError( "You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup");
			intent = new Intent();
			intent.setAction( Intent.ACTION_VIEW );
			intent.setData( Uri.parse("https://twitter.com/intent/tweet").buildUpon().appendQueryParameter("text",message).build() );
			startActivity( intent );
		}
	}



	// -- NATIVE CODE ------------------------------------------------------------------------------

	void initNativeLib()
	{
		try {
			PackageManager pm  = this.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(getPackageName(),0);
			ApplicationInfo ai = pm.getApplicationInfo(getPackageName(),0);

			String version = pi.versionName;
			String name = (String)pm.getApplicationLabel(ai);

			if ( name!=null && version!=null ) initNativeLib( name, version );
		}
		catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
	}


	native void initNativeLib( String name, String version );

	static {
		try {
			System.loadLibrary("ogg");
			System.loadLibrary("json");
			System.loadLibrary("vorbis");
			System.loadLibrary("locuscast");
		}
		catch (UnsatisfiedLinkError e) {
			System.err.println("Native libraries failed to load: " + e);
			System.exit(1);
		}
	}

}

