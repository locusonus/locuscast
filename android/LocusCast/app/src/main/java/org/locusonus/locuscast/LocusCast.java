/**
 *	LocusCast.java
 *	LocusCast
 *
 *
 *  LocusCast is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using LocusCast please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  LocusCast was designed Locus Sonus <http://locusonus.org/>.
 *  LocusCast is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */
package org.locusonus.locuscast;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;

import java.util.ArrayList;
import java.util.List;





public class LocusCast
{


    public static final int REQUEST_ALL_PERMISSIONS = 0;
    public static final int REQUEST_AUDIO_PERMISSION = 1;
    public static final int REQUEST_LOCATION_PERMISSION = 2;

    private static boolean askForAudioPermission = true;
    private static boolean askForLocationPermission = true;
    private static boolean audioPermissionGranted = true;
    private static boolean locationPermissionGranted = true;

    public static final int MAX_AUTO_RECONNECTION = 10;
    public static final int CHECK_LIVE_SLEEP_SEC  = 3*1000;


    private static Activity context;

    private static List<OnPermissionListener> permissionListeners;
    private static List<OnStreamListener> streamListeners;




    public static void register( Activity activity )
    {
        context = activity;

        permissionListeners = new ArrayList<OnPermissionListener>();
        streamListeners = new ArrayList<OnStreamListener>();
    }



    // -- INTERFACE  -------------------------------------------------------------------------------

    public interface OnPermissionListener {
        void OnGrantedAudioPermission( boolean granted );
        void OnGrantedLocationPermission( boolean granted );
    }

    public interface OnStreamListener {
        void onRequestStopStream();
        void onStreamIsStopped();
    }



    // -- MESSAGE / WARNING  -----------------------------------------------------------------------

    static public void displayError(String message)
    {
        displayError( context.getString(R.string.app_name), message );
    }

    static public void displayError(final String title, final String message )
    {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(context)
                        .setTitle(title)
                        .setMessage( message )
                        .setPositiveButton(android.R.string.ok, null)
                        .setIcon(R.mipmap.ic_launcher)
                        .show();
            }
        });
    }

    static public void onOutOfMemoryError(OutOfMemoryError e)
    {
        stopStream();
        displayError("Out of memory error.\n"+e.getMessage());
    }



    // -- UI --------------------------------------------------------------------------------

    public static int getTabWidth()
    {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.widthPixels / 5;
    }



    // -- FORMATTER --------------------------------------------------------------------------------

    static String timeFormatted( long ms )
    {
        int seconds = (int) ((ms / 1000) % 60);
        int minutes = (int) ((ms / 60000) % 60);
        int hours   = (int) ((ms / 3600000) % 24);

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    static String timeFormattedWithSecond( long sec )
    {
        return timeFormatted(sec*1000);
    }

    static String timeFormattedWithMilliseconds( long ms )
    {
        int millis  = (int) ((ms) % 1000);
        int seconds = (int) ((ms / 1000) % 60);
        int minutes = (int) ((ms / 60000) % 60);
        int hours   = (int) ((ms / 3600000) % 24);
        //long days   = (ms / 86400000 );

        return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, millis);
    }



    // -- PERMISSIONS ------------------------------------------------------------------------------

    static public boolean askForAllRequiredPermissions()
    {
        if (
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)!=PackageManager.PERMISSION_GRANTED
         ) {

            final String[] permissions = new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

            requestPermission(permissions, REQUEST_ALL_PERMISSIONS);
            askForLocationPermission = false;
            askForAudioPermission = false;

            return false;
        }

        return true;
    }

    static public boolean hasLocationPermission()
    {
        if (
                askForLocationPermission &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED
           ) {

            final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

            requestPermission(permissions, REQUEST_LOCATION_PERMISSION);
            askForLocationPermission = false;

            return false;
        }

        return locationPermissionGranted;
    }

    static public boolean hasAudioPermission()
    {
        if (
                askForAudioPermission &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)!=PackageManager.PERMISSION_GRANTED
        ) {

            final String[] permissions = new String[]{Manifest.permission.RECORD_AUDIO};

            requestPermission(permissions, REQUEST_AUDIO_PERMISSION);
            askForAudioPermission = false;

            return false;
        }

        return audioPermissionGranted;
    }


    static private void requestPermission( String[] permissions, int requestCode ) {
        ActivityCompat.requestPermissions(context, permissions, requestCode);
    }

    static public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ALL_PERMISSIONS :
                audioPermissionGranted    = ( grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED );
                locationPermissionGranted = ( grantResults.length>1 && grantResults[1]==PackageManager.PERMISSION_GRANTED );
                for( OnPermissionListener listener : permissionListeners ) {
                    listener.OnGrantedAudioPermission(audioPermissionGranted);
                    listener.OnGrantedLocationPermission(locationPermissionGranted);
                }
                break;
            case REQUEST_AUDIO_PERMISSION :
                audioPermissionGranted = ( grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED );
                for( OnPermissionListener listener : permissionListeners ) listener.OnGrantedAudioPermission(audioPermissionGranted);
                break;
            case REQUEST_LOCATION_PERMISSION :
                locationPermissionGranted = ( grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED );
                for( OnPermissionListener listener : permissionListeners ) listener.OnGrantedLocationPermission(locationPermissionGranted);
                break;
        }
    }

    static public void setOnPermissionListener( OnPermissionListener listener )
    {
        permissionListeners.add(listener);
    }



    // -- AUDIO ------------------------------------------------------------------------------------

    static public void stopStream()
    {
        for( OnStreamListener listener : streamListeners ) listener.onRequestStopStream();
    }

    static public void notifyStreamIsStopped()
    {
        for( OnStreamListener listener : streamListeners ) listener.onStreamIsStopped();
    }

    static public void setOnStreamListener( OnStreamListener listener )
    {
        streamListeners.add(listener);
    }



    // -- STREAM (NATIVE) --------------------------------------------------------------------------

    public static boolean isConfigured()
    {
        return streamID()!=0;
    }

    public static native int streamID();

    public static native boolean autoconfig( String name, String pass );
    public static native String getUserConfiguration();
    public static native String getAppInformation();
    public static native String getLastError();

    public static native String getName();
    public static native String getUrl();
    public static native String getMountpoint();
    public static native int getChannel();
    public static native int getSampleRate();

    public static native void setChannel(int channel);
    public static native void setQuality(float quality);
    public static native void setSampleRate(int sampleRate);
    public static native void setBufferDuration(int duration);

    public static native boolean streamInit();
    public static native boolean streamStart();
    public static native boolean streamStop();
    public static native boolean streamIsEncoding();
    public static native boolean streamIsRunning();
    public static native boolean streamIsMounted();
    public static native boolean sendLocation( double lat, double lng );

    public static native long streamGetTransferedBytes();
    public static native long streamGetTransferedPackets();

}
