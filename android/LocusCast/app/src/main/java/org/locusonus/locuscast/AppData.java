/**
 *	AppData.java
 *	liveSHOUT
 *
 *
 *  liveSHOUT is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using liveSHOUT please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  liveSHOUT was designed at the Sonic Arts Research Centre <http://www.sarc.qub.ac.uk> in
 *  partnership with Locus Sonus <http://locusonus.org/> and was made possible through an Arts and
 *  Humanities Research Council research grant led by Dr Franziska Schroeder and Professor Pedro
 *  Rebelo at Queen’s University Belfast.
 *  liveSHOUT is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */
package org.locusonus.locuscast;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class AppData {


    // preference name
    public static final String PREFERENCES = "LocusCastData";

    // default audio settings
    public static final int DEFAULT_INPUTCHANNEL    = -1;
    public static final int DEFAULT_SAMPLEREATE     = 44100;
    public static final int DEFAULT_BUFFERSEC       = 3;
    public static final float DEFAULT_AUDIOQUALITY  = 0.3f;

    // The amount of time between location updates, in seconds
    // minimum distance between location updates, in meters
    public static final int DEFAULT_LOC_TIMEOUT     = 10;
    public static final int DEFAULT_LOC_DISTANCE    = 100;


    static SharedPreferences preferences;

    // data
    public static String logname;
    public static String logpass;

    public static int devchannel;   // device input channels
    public static int auchannel;
    public static int ausamplerate;
    public static int aubuffersize;
    public static float auquality;

    public static int loctime;
    public static int locdist;

    public static boolean autoreconnect;

    public static boolean hasChanged;



    public static void register( Activity activity )
    {

        preferences = activity.getSharedPreferences(PREFERENCES,0);

        logname = "";
        logpass = "";
        devchannel = 1;
        auchannel = DEFAULT_INPUTCHANNEL;
        ausamplerate = DEFAULT_SAMPLEREATE;
        aubuffersize = DEFAULT_BUFFERSEC;
        auquality = DEFAULT_AUDIOQUALITY;
        loctime = DEFAULT_LOC_TIMEOUT;
        locdist = DEFAULT_LOC_DISTANCE;
        autoreconnect = true;
        hasChanged = false;
    }


    public static void markUnchanged()
    {
        hasChanged = false;
    }

    private static void saveString( String key, String value )
    {
        try {
            if ( preferences!=null) {
                Editor editor = preferences.edit();
                editor.putString(key,value);
                editor.commit();
            }
        }
        catch( Exception e ) { LocusCast.displayError("Failed to save preferences"); }
    }

    private static void saveFloat( String key, float value )
    {
        try {
            if ( preferences!=null) {
                Editor editor = preferences.edit();
                editor.putFloat(key,value);
                editor.commit();
            }
        }
        catch( Exception e ) { LocusCast.displayError("Failed to save preferences"); }
    }

    private static void saveInt( String key, int value )
    {
        try {
            if ( preferences!=null) {
                Editor editor = preferences.edit();
                editor.putInt(key,value);
                editor.commit();
            }
        }
        catch( Exception e ) { LocusCast.displayError("Failed to save preferences"); }
    }

    private static void saveBoolean( String key, boolean value )
    {
        try {
            if ( preferences!=null) {
                Editor editor = preferences.edit();
                editor.putBoolean(key,value);
                editor.commit();
            }
        }
        catch( Exception e ) { LocusCast.displayError("Failed to save preferences"); }
    }



    // -- account ----------------------------------------------------------------------------------

    public static void loadLogin()
    {
        if ( preferences!=null) {
            logname = preferences.getString("kLogname", "");
            logpass = preferences.getString("kLogpass", "");
        }
    }

    public static void saveLogin( String name, String pass )
    {
        if ( name.equals(logname) && pass.equals(logpass) ) return;

        hasChanged = true;
        logname = name.trim();
        logpass = pass.trim();

        saveString( "kLogname", logname );
        saveString( "kLogpass", logpass );

    }

    public static boolean isEmpty()
    {
        return logname.isEmpty() && logpass.isEmpty();
    }



    // -- audio ------------------------------------------------------------------------------------

    public static void loadAudioSettings()
    {
        if ( preferences!=null) {
            auchannel = preferences.getInt("kAudioChannel", DEFAULT_INPUTCHANNEL);
            ausamplerate = preferences.getInt("kAudioSamplerate", DEFAULT_SAMPLEREATE);
            aubuffersize = preferences.getInt("kAudioBufferSize", DEFAULT_BUFFERSEC);
            auquality = preferences.getFloat("kAudioQuality", DEFAULT_AUDIOQUALITY);

            if (auchannel == DEFAULT_INPUTCHANNEL) auchannel = devchannel;
        }
    }

    public static void saveAudioInputQuality(float quality)
    {
        if ( quality==auquality ) return;

        hasChanged = true;
        auquality  = quality;
        saveFloat("kAudioQuality", quality);
    }

    public static void saveAudioInputBuffer(int second)
    {
        if ( second==aubuffersize ) return;

        hasChanged = true;
        aubuffersize = second;
        saveInt("kAudioBufferSize", second);
    }

    public static void updateDeviceInputChannel(int channel)
    {
        devchannel = Math.min(1,Math.max(channel,2));
    }

    public static void saveAudioInputChannel(int channel)
    {
        if ( channel==auchannel ) return;

        hasChanged = true;
        auchannel = channel==DEFAULT_INPUTCHANNEL ? devchannel : channel;
        saveInt("kAudioChannel", channel);
    }

    public static void saveAudioInputSamplerate(int samplerate)
    {
        if ( samplerate==ausamplerate ) return;

        hasChanged = true;
        ausamplerate = samplerate;
        saveInt("kAudioSamplerate", samplerate);
    }



    // -- location ---------------------------------------------------------------------------------

    public static void loadLocationSettings()
    {
        if ( preferences!=null) {
            locdist = preferences.getInt("kLocationDistance", DEFAULT_LOC_DISTANCE);
            loctime = preferences.getInt("kLocationTime", DEFAULT_LOC_TIMEOUT);
        }
    }

    public static void saveLocationTime(int time)
    {
        if ( time==loctime ) return;

        hasChanged = true;
        loctime = time;
        saveInt("kLocationTime", time);
    }

    public static void saveLocationDistance(int distance)
    {
        if ( distance==locdist ) return;

        hasChanged = true;
        locdist = distance;
        saveInt("kLocationDistance", distance);
    }



    // -- advanced settings ------------------------------------------------------------------------

    public static void loadStreamSettings()
    {
        if ( preferences!=null) {
            autoreconnect = preferences.getBoolean("kStreamAutoReconnect", true);
        }
    }

    public static void saveStreamAutoReconnect(boolean allow)
    {
        if ( allow==autoreconnect ) return;

        hasChanged = true;
        autoreconnect = allow;
        saveBoolean("kStreamAutoReconnect", allow);

    }
}
