/**
 *	ConfigAudioViewFragment.java
 *	liveSHOUT
 *
 *
 *  liveSHOUT is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using liveSHOUT please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  liveSHOUT was designed at the Sonic Arts Research Centre <http://www.sarc.qub.ac.uk> in
 *  partnership with Locus Sonus <http://locusonus.org/> and was made possible through an Arts and
 *  Humanities Research Council research grant led by Dr Franziska Schroeder and Professor Pedro
 *  Rebelo at Queen’s University Belfast.
 *  liveSHOUT is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */
package org.locusonus.locuscast;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;

import java.util.Arrays;


public class ConfigAudioViewFragment extends Fragment implements OnSeekBarChangeListener, OnCheckedChangeListener, OnItemSelectedListener
{

    final String[] samplerates = {"8000", "11025", "16000", "22050", "32000", "44100", "48000"};

    Switch auchannel;

    SeekBar auquality;
    Spinner ausamplerate;

    TextView auqualityV;

    Activity context;


    ConfigAudioViewFragment(Activity activity) {
        context = activity;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppData.loadAudioSettings();
        AppData.markUnchanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.config_audio_layout, container, false);

        auchannel     = (Switch)view.findViewById(R.id.auchannel);
        auquality     = (SeekBar)view.findViewById(R.id.auquality);
        auqualityV    = (TextView)view.findViewById(R.id.auqualityV);
        ausamplerate  = (Spinner)view.findViewById(R.id.ausamplerate);

        auchannel.setChecked(AppData.auchannel==2);
        auchannel.setOnCheckedChangeListener(this);

        auquality.setProgress((int)(AppData.auquality*10));
        auquality.setOnSeekBarChangeListener(this);
        auqualityV.setText(String.format("%.1f", AppData.auquality));


        int index = Arrays.asList(samplerates).indexOf( Integer.toString(AppData.ausamplerate) );
        ArrayAdapter<String> data = new ArrayAdapter<String>(context, R.layout.samplerate_spinner_item, R.id.samplerate, samplerates);
        data.setDropDownViewResource(R.layout.samplerate_spinner_dropdown);

        ausamplerate.setAdapter(data);
        ausamplerate.setSelection(index, false);
        ausamplerate.setOnItemSelectedListener(this);


        return view;
    }



    // -- SWITCH -----------------------------------------------------------------------------------

    @Override
    public void onCheckedChanged(CompoundButton elem, boolean isChecked)
    {
        switch (elem.getId()){
            case R.id.auchannel:
                AppData.saveAudioInputChannel(isChecked?2:1);
                break;
        }
    }



    // -- SLIDER -----------------------------------------------------------------------------------

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b)
    {
        switch (seekBar.getId()){
            case R.id.auquality:
                auqualityV.setText(String.format("%.1f", progress/10.f));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {
        int value = seekBar.getProgress();

        switch (seekBar.getId()){
            case R.id.auquality:
                AppData.saveAudioInputQuality(value/10.f);
                break;
        }

        clearSeekBarMessage(seekBar);
    }

    private void clearSeekBarMessage(SeekBar seekBar)
    {
    }



    // -- SLIDER -----------------------------------------------------------------------------------

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int index, long id) {
        int samplerate = Integer.parseInt(samplerates[index]);
        AppData.saveAudioInputSamplerate(samplerate);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}