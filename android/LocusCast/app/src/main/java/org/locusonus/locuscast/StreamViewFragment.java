/**
 *	StreamViewFragment.java
 *	liveSHOUT
 *
 *
 *  liveSHOUT is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using liveSHOUT please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  liveSHOUT was designed at the Sonic Arts Research Centre <http://www.sarc.qub.ac.uk> in
 *  partnership with Locus Sonus <http://locusonus.org/> and was made possible through an Arts and
 *  Humanities Research Council research grant led by Dr Franziska Schroeder and Professor Pedro
 *  Rebelo at Queen’s University Belfast.
 *  liveSHOUT is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */
package org.locusonus.locuscast;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


public class StreamViewFragment extends Fragment  implements
        OnClickListener, LocationListener,
        LocusCast.OnStreamListener,
        LocusCast.OnPermissionListener
{

    // type of information on the audio stream
    protected static final int INFO_TYPE_TIME   = 0;	// elapsed time
    protected static final int INFO_TYPE_BYTES  = 1;	// bytes sent
    protected static final int INFO_TYPE_PACKET = 2;	// packet sent
    protected static final int INFO_TYPE_END    = 3;

    // type of message for the current stream
    protected static final int MSG_TYPE_LOCATON = 0;	// current location
    protected static final int MSG_TYPE_NAME    = 1;	// current stream name
    protected static final int MSG_TYPE_URL     = 2;    // current stream URL
    protected static final int MSG_TYPE_MPOINT  = 3;    // current stream mountpoint
    protected static final int MSG_TYPE_END     = 4;


    //Chronometer chrono;
    Button button;

    TextView message;
    TextView infoText;
    TextView infoType;

    Activity context;

    LocationManager locationManager;
    String locationProvider;
    String locationMessage;

    int infoCycle;
    int currentStreamID;
    int reconnectCount;
    boolean isRunning;
    boolean isStereo;
    float gain;
    long time;


    public StreamViewFragment(Activity activity)
    {
        this.context = activity;

        streamSetup();
        locationSetup();

        LocusCast.setOnStreamListener(this);
        LocusCast.setOnPermissionListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocusCast.askForAllRequiredPermissions();

        // try to sync again
        if ( !LocusCast.isConfigured() ) streamGetConfiguration();

        // load latest settings first
        AppData.loadLogin();
        AppData.loadAudioSettings();
        AppData.loadStreamSettings();
        AppData.loadLocationSettings();

        // prepare stream
        LocusCast.setChannel( AppData.auchannel );
        LocusCast.setQuality( AppData.auquality );
        LocusCast.setSampleRate( AppData.ausamplerate );
        LocusCast.setBufferDuration( AppData.aubuffersize );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.stream_layout, container, false);

        button   = view.findViewById(R.id.remote);
        message  = view.findViewById(R.id.message);
        infoText = view.findViewById(R.id.info);
        infoType = view.findViewById(R.id.infotype);

        message.setText(LocusCast.getName());

        button.setEnabled( !AppData.isEmpty() && LocusCast.isConfigured() && LocusCast.hasAudioPermission() );
        button.setTextColor( button.isEnabled() ? R.color.tint : R.color.disable );
        button.setOnClickListener(this);

        ((Button)view.findViewById(R.id.open_config_panel)).setOnClickListener(this);

        infoInit();

        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }


    @Override
    public void OnGrantedAudioPermission(boolean granted)
    {
        if ( !granted && button!=null ) button.setEnabled(false);
    }

    @Override
    public void OnGrantedLocationPermission(boolean granted) {}

    @Override
    public void onClick(View view) {
        switch( view.getId() ){

            // setup action
            case R.id.open_config_panel:
                streamStop();
                ((MainActivity)context).switchToConfigView();
                break;

            // info action
            case R.id.info:
                infoSwitch();
                break;

            // remote action
            case R.id.remote:
                if ( button.isEnabled() ) {
                    if (isRunning) streamStop();
                    else streamStart();
                }
                break;
        }
    }


    public void displayMessage( final String msg )
    {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                message.setText( msg );
            }
        });
    }



    // -- STREAM -----------------------------------------------------------------------------------

    @Override
    public void onRequestStopStream()
    {
        streamStop();
    }

    @Override
    public void onStreamIsStopped() {}

    void streamSetup()
    {
        currentStreamID = 0;
        reconnectCount = 0;
        isRunning = false;
        isStereo = AppData.auchannel==2;
        gain = 1.f;
    }

    void streamButtonUpdate()
    {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String label = context.getString( isRunning ? R.string.stop : R.string.start );
                button.setText(label);
            }
        });
    }

    void streamStart()
    {
        if ( isRunning ) return;

        isStereo  = LocusCast.getChannel()==2;
        isRunning = LocusCast.streamInit() && LocusCast.streamStart();
        if ( isRunning ) {

            displayMessage("Initialize audio encoder");

            locationStart();
            infoStart();

            streamButtonUpdate();
            streamCheckForError();

        }
        else LocusCast.displayError( LocusCast.getLastError() );
    }

    void streamStop()
    {
        if ( !isRunning ) return;

        isStereo  = LocusCast.getChannel()==2; // if conf change
        isRunning = false;

        locationStop();
        LocusCast.streamStop();

        // wait for encoder stop
        while( LocusCast.streamIsEncoding() );
        LocusCast.notifyStreamIsStopped();

        streamButtonUpdate();
    }

    void streamCheckForError()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {

                reconnectCount = 0;
                currentStreamID = LocusCast.streamID();

                while( isRunning ) {

                    // check for encoding error
                    if ( isRunning && (!LocusCast.streamIsRunning() || !LocusCast.streamIsEncoding()) ) {
                        String err = LocusCast.getLastError();
                        LocusCast.displayError( err.isEmpty() ? "Audio encoder stopped unexpectedly" : err );
                        streamStop();
                    }

                    try { Thread.sleep(LocusCast.CHECK_LIVE_SLEEP_SEC); }
                    catch( InterruptedException e ) {}

                    // check for broken connection
                    if ( isRunning && !LocusCast.streamIsMounted() ) {
                        if ( AppData.autoreconnect && reconnectCount<LocusCast.MAX_AUTO_RECONNECTION ) {
                            LocusCast.streamStop();
                            LocusCast.streamStart();
                            reconnectCount++;
                            continue;
                        }
                        else {
                            LocusCast.displayError("Broken connection");
                            streamStop();
                        }
                    }
                }
            }
        }).start();
    }

    void streamGetConfiguration()
    {
        AppData.loadLogin();
        if ( !AppData.isEmpty() ) {
            boolean isLogged = LocusCast.autoconfig(AppData.logname,AppData.logpass);
            if ( !isLogged ) LocusCast.displayError(LocusCast.getLastError());
        }

    }



    // -- INFO -------------------------------------------------------------------------------------

    void infoInit()
    {
        infoCycle = INFO_TYPE_TIME;

        infoText.setOnClickListener(this);
        infoStop();
    }

    void infoStart()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {

                time = 0;

                int cycle = 0;
                while( !Thread.interrupted() && isRunning ) {
                    try {

                        infoUpdate();
                        Thread.sleep(1000);
                        time++;

                        // messages cycle
                        if ( time%7==0 ) {
                            if ( ++cycle==MSG_TYPE_END ) cycle = 0;
                            switch(cycle) {
                                case MSG_TYPE_LOCATON:
                                    displayMessage(locationMessage);
                                    break;
                                case MSG_TYPE_NAME:
                                    displayMessage(String.format("stream name\n%s", LocusCast.getName()));
                                    break;
                                case MSG_TYPE_URL:
                                    displayMessage(String.format("stream url\n%s", LocusCast.getUrl()));
                                    break;
                                case MSG_TYPE_MPOINT:
                                    displayMessage(String.format("mountpoint\n%s", LocusCast.getMountpoint()));
                                    break;
                            }
                        }

                    } catch (InterruptedException e) {}
                }

                infoStop();

            }
        }).start();
    }

    void infoUpdate()
    {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String text = "";
                String type = "";

                switch( infoCycle )
                {
                    case INFO_TYPE_BYTES  :
                        long bytes = LocusCast.streamGetTransferedBytes();
                        text = Formatter.formatFileSize(context, bytes);
                        type = String.format("%,d bytes", bytes);
                        break;

                    case INFO_TYPE_PACKET :
                        text = String.format("%d",LocusCast.streamGetTransferedPackets());
                        type = "packets";
                        break;

                    default:
                        text = LocusCast.timeFormattedWithSecond(time);
                        type = "";
                        break;
                }

                infoText.setText(text);
                infoType.setText(type);
            }
        });
    }

    void infoStop()
    {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                infoText.setText(context.getString(R.string.stopwatch));
                infoType.setText("");

                // TODO: add background mode
                context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
    }

    void infoSwitch()
    {
        if ( isRunning ) {
            infoCycle += 1;
            infoCycle %= INFO_TYPE_END;
            infoUpdate();
        }
    }



    // -- LOCATION ---------------------------------------------------------------------------------

    public void locationSetup()
    {
        try {
            locationMessage = "";
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        } catch (Exception e) {
            locationManager = null;
            LocusCast.displayError(e.getMessage());
        }

        // location Provider criteria
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setSpeedRequired(true);

        locationProvider = locationManager!=null ? locationManager.getBestProvider(criteria, true) : null;
    }
    public void locationStart()
    {
        if ( LocusCast.hasLocationPermission() ) {
            if ( locationProvider!=null ) {
                locationManager.requestLocationUpdates(locationProvider, AppData.loctime, AppData.locdist, this);
            }
            else LocusCast.displayError("No Location Providers currently available");
        }
    }
    public void locationStop()
    {
        if ( locationManager!=null ) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        double lat = location.getLatitude();
        double lon = location.getLongitude();

        String str = LocusCast.sendLocation(lat,lon) ? String.format("latitude: %g - longitude: %g",lat,lon) : "Failed to save your location:\n"+LocusCast.getLastError();

        // display message on change
        if ( !locationMessage.equals(str) ) {
            displayMessage( String.format("%s", str) );
            locationMessage = str;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle bundle) {}

    @Override
    public void onProviderEnabled(String s) {}

    @Override
    public void onProviderDisabled(String s) {}
}
