/**
 *	ConfigViewFragment.java
 *	liveSHOUT
 *
 *
 *  liveSHOUT is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using liveSHOUT please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  liveSHOUT was designed at the Sonic Arts Research Centre <http://www.sarc.qub.ac.uk> in
 *  partnership with Locus Sonus <http://locusonus.org/> and was made possible through an Arts and
 *  Humanities Research Council research grant led by Dr Franziska Schroeder and Professor Pedro
 *  Rebelo at Queen’s University Belfast.
 *  liveSHOUT is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */
package org.locusonus.locuscast;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;



public class ConfigViewFragment extends Fragment implements Runnable, OnClickListener, OnCheckedChangeListener, OnEditorActionListener
{

    EditText logname;
    EditText logpass;

    TextView loginfo;

    ProgressBar progress;

    Switch autoreconnect;

    Thread thread;
    Activity context;


    ConfigViewFragment(Activity activity)
    {
        context = activity;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppData.loadLogin();
        AppData.loadStreamSettings();
        AppData.markUnchanged();
    }

   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.config_layout, container, false);
        view.setBackgroundColor( Color.TRANSPARENT );

        logname   = view.findViewById(R.id.logname);
        logpass   = view.findViewById(R.id.logpass);
        loginfo   = view.findViewById(R.id.loginfo);
        progress  = view.findViewById(R.id.logprogress);

        logname.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        logname.setText( AppData.logname );
        logpass.setText( AppData.logpass );
        logpass.setOnEditorActionListener(this);

        displayLogin();

        autoreconnect = (Switch)view.findViewById(R.id.autoreconnect);
        autoreconnect.setChecked(AppData.autoreconnect);
        autoreconnect.setOnCheckedChangeListener(this);

        ((Button)view.findViewById(R.id.login)).setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if ( AppData.hasChanged && LocusCast.streamIsEncoding() )
            LocusCast.displayError("changes will take effect when stream restarts");
    }



    // -- EDITTEXT ---------------------------------------------------------------------------------

    @Override
    public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
        if ( id==EditorInfo.IME_ACTION_SEND) {

            // start login process
            progress.setAlpha(1.0f);
            if ( thread==null ) {
                thread = new Thread( this );
                thread.start();
            }

            InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(textView.getWindowToken(), 0);

            return true;
        }
        return false;
    }



    // -- BUTTON -----------------------------------------------------------------------------------

    @Override
    public void onClick(View view)
    {
        switch( view.getId() ){
            case R.id.login:
                onEditorAction(logpass, EditorInfo.IME_ACTION_SEND, null);
                break;
        }
    }



    // -- SWITCH -----------------------------------------------------------------------------------

    @Override
    public void onCheckedChanged(CompoundButton elem, boolean isChecked)
    {
        switch (elem.getId()){
            case R.id.autoreconnect:
                AppData.saveStreamAutoReconnect(isChecked);
                break;
        }
    }



    // -- LOGIN ------------------------------------------------------------------------------------


    void displayLogin()
    {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                String msg = context.getString(R.string.login_info);
                if ( !AppData.isEmpty() ) msg += String.format(" :\n%s", LocusCast.getName());

                progress.setAlpha(0);
                loginfo.setText(msg);
            }
        });
    }

    @Override
    public void run()
    {

        String name = logname.getText().toString().trim();
        String pass = logpass.getText().toString().trim();

        if ( !name.isEmpty() && !pass.isEmpty() ) {

            boolean logged = LocusCast.autoconfig(name,pass);
            if ( logged ) {

                AppData.saveLogin(name,pass);
                LocusCast.stopStream();

                ((MainActivity)context).switchToStreamView();
            }
            else LocusCast.displayError(LocusCast.getLastError());
        }

        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setAlpha(0);
            }
        });
        thread = null;
    }
}
