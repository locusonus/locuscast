/**
 *	LocusCast.cpp
 *	liveSHOUT
 *
 *
 *  liveSHOUT is an interactive audio streaming mobile app. The app is designed to facilitate
 *  multiple streaming points and multiple listening points, what we call "distributed listening".
 *  For more information, user manual and links to projects using liveSHOUT please visit
 *  http://www.socasites.qub.ac.uk/distributedlistening/
 *
 *
 *  liveSHOUT was designed at the Sonic Arts Research Centre <http://www.sarc.qub.ac.uk> in
 *  partnership with Locus Sonus <http://locusonus.org/> and was made possible through an Arts and
 *  Humanities Research Council research grant led by Dr Franziska Schroeder and Professor Pedro
 *  Rebelo at Queen’s University Belfast.
 *  liveSHOUT is written by Stéphane Cousot <http://www.ubaa.net/> and released under GNU General
 *  Public License, version 3 or later.
 *
 *  Many thanks to Rafael Diniz and DarkIce Team for the upstream audio engine.
 *  DarkIce is © Copyright Tyrell Hungary and Rafael Diniz and Ákos Maróy under the GNU General
 *  Public Licence version 3. <http://www.darkice.org>
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *	------------------------------------------------------------------------------------------------
 */



#include <jni.h>
#include <string>

#include "LocusIce.h"



const char* _appname = "locuscast";
const char* _appversion = "unknown";


const char* getAppName()
{
    return _appname;
}

const char* getAppVersion()
{
    return _appversion;
}



extern "C"
{

	/**
	 * MainActivity.java
	 */
	JNIEXPORT void JNICALL Java_org_locusonus_locuscast_MainActivity_initNativeLib(JNIEnv *env, jobject obj, jstring name, jstring version)
	{
	
		_appname = env->GetStringUTFChars(name,0);
		_appversion = env->GetStringUTFChars(version,0);
	
		#ifdef DEBUG
		log(
				"\n------------------\n%s\n------------------\n",
				getAppInformation().c_str()
		);
		#endif
	
	}
	
	
	
	/**
	 * LocusCast.java
	 */
	JNIEXPORT jint JNICALL Java_org_locusonus_locuscast_LocusCast_streamID(JNIEnv *env, jclass type)
	{
		return streamID();
	}
	
	JNIEXPORT jstring JNICALL Java_org_locusonus_locuscast_LocusCast_getName(JNIEnv* env, jobject obj)
	{
		const char * msg = getName();
		return env->NewStringUTF(msg);
	}
	
	JNIEXPORT jstring JNICALL Java_org_locusonus_locuscast_LocusCast_getUrl(JNIEnv* env, jobject obj)
	{
		const char * msg = getUrl();
		return env->NewStringUTF(msg);
	}
	
	JNIEXPORT jstring JNICALL Java_org_locusonus_locuscast_LocusCast_getMountpoint(JNIEnv* env, jobject obj)
	{
		const char * msg = getMountpoint();
		return env->NewStringUTF(msg);
	}
	
	JNIEXPORT jstring JNICALL Java_org_locusonus_locuscast_LocusCast_getLastError(JNIEnv* env, jobject obj)
	{
		const char * msg = getLastError();
		return env->NewStringUTF(msg);
	}
	
	JNIEXPORT jint JNICALL Java_org_locusonus_locuscast_LocusCast_getChannel(JNIEnv* env, jobject obj)
	{
		return getChannel();
	}
	
	JNIEXPORT jint JNICALL Java_org_locusonus_locuscast_LocusCast_getSampleRate(JNIEnv* env, jobject obj)
	{
		return getSampleRate();
	}
	
	JNIEXPORT void JNICALL Java_org_locusonus_locuscast_LocusCast_setChannel(JNIEnv *env, jclass type, jint channel)
	{
		setChannel(channel);
	}
	
	JNIEXPORT void JNICALL Java_org_locusonus_locuscast_LocusCast_setQuality(JNIEnv *env, jclass type, jfloat quality)
	{
		setQuality(quality);
	}
	
	JNIEXPORT void JNICALL Java_org_locusonus_locuscast_LocusCast_setSampleRate(JNIEnv *env, jclass type, jint sampleRate)
	{
		setSampleRate(sampleRate);
	}
	
	JNIEXPORT void JNICALL Java_org_locusonus_locuscast_LocusCast_setBufferDuration(JNIEnv *env, jclass type,jint duration)
	{
		setBufferDuration(duration);
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_autoconfig(JNIEnv* env, jobject obj, jstring name, jstring pass)
	{
		const char * n  = env->GetStringUTFChars(name,0);
		const char * p  = env->GetStringUTFChars(pass,0);
		jboolean result = autoconfig(n,p);
	
		// FIXME: something strange in autoconfig if not use the onResult ?
		// ANDROID bug only
		char msg[2048]; sprintf( msg, "auto configured: %s", result ? "true":"false" );
	
		return result;
	}

	JNIEXPORT jstring JNICALL Java_org_locusonus_locuscast_LocusCast_getUserConfiguration(JNIEnv* env, jobject obj)
	{
		// FIXME: sometimes  empty using const char* ?
		/*const char * msg = getUserConfiguration();
		return env->NewStringUTF(msg);*/
		return env->NewStringUTF( getUserConfiguration().c_str() );
	}

	JNIEXPORT jstring JNICALL Java_org_locusonus_locuscast_LocusCast_getAppInformation(JNIEnv* env, jobject obj)
	{
		// FIXME: sometimes  empty using const char* ?
		/*const char * msg = getAppInformation();
		return env->NewStringUTF(msg);*/
		return env->NewStringUTF( getAppInformation().c_str() );
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamInit(JNIEnv* env, jobject obj)
	{
		return streamInit();
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamStart(JNIEnv* env, jobject obj)
	{
		return streamStart();
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamStop(JNIEnv* env, jobject obj)
	{
		return streamStop();
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamWrite(JNIEnv* env, jobject obj, jshortArray buffer, jint bufferSize )
	{
	
		short* buf = (short*)env->GetPrimitiveArrayCritical(buffer, NULL);
		if ( buf==NULL ) return false;
	
		streamWrite( buf, bufferSize );
		env->ReleasePrimitiveArrayCritical( buffer, buf, JNI_OK );
	
		return true;
	}
	
	JNIEXPORT void JNICALL Java_org_locusonus_locuscast_LocusCast_streamSetInputGain(JNIEnv *env, jclass type, jfloat gain)
	{
		streamSetInputGain(gain);
	}
	
	JNIEXPORT jfloat JNICALL Java_org_locusonus_locuscast_LocusCast_streamGetInternalBufferStatus(JNIEnv *env, jclass type)
	{
		return streamGetInternalBufferStatus();
	}
	
	JNIEXPORT jfloat JNICALL Java_org_locusonus_locuscast_LocusCast_streamGetInputLevel(JNIEnv *env, jclass type, jint channel)
	{
		return streamGetInputLevel(channel);
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamIsEncoding(JNIEnv* env, jobject obj)
	{
		return streamIsEncoding();
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamIsRunning(JNIEnv* env, jobject obj)
	{
		return streamIsRunning();
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_streamIsMounted(JNIEnv* env, jobject obj)
	{
		return streamIsMounted();
	}
	
	JNIEXPORT jlong JNICALL Java_org_locusonus_locuscast_LocusCast_streamGetTransferedPackets(JNIEnv* env, jobject obj)
	{
		return streamGetTransferedPackets();
	}
	JNIEXPORT jlong JNICALL Java_org_locusonus_locuscast_LocusCast_streamGetTransferedSamples(JNIEnv* env, jobject obj)
	{
		return streamGetTransferedSamples();
	}
	JNIEXPORT jlong JNICALL Java_org_locusonus_locuscast_LocusCast_streamGetTransferedBytes(JNIEnv* env, jobject obj)
	{
		return streamGetTransferedBytes();
	}
	
	JNIEXPORT jboolean JNICALL Java_org_locusonus_locuscast_LocusCast_sendLocation(JNIEnv* env, jobject obj, jdouble lat, jdouble lng )
	{
		return sendLocation(lat,lng);
	}
	
	JNIEXPORT jint JNICALL Java_org_locusonus_locuscast_LocusCast_getOpenSLESMiniumGain(JNIEnv *env, jobject instance)
	{
		return OPENSLES_GAIN_MIN;
	}
	
	JNIEXPORT jint JNICALL Java_org_locusonus_locuscast_LocusCast_getOpenSLESMaximumGain(JNIEnv *env, jobject instance)
	{
		return OPENSLES_GAIN_MAX;
	}

}
