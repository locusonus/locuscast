/**
 *	MainController.h
 *	Stéphane Cousot @ http://www.ubaa.net/
 *	LocusCast is released under GNU General Public License, version 3 or later
 *
 *
 *	This file is part of the LocusCast application.
 *	LocusCast is a mobile audio streaming app for Locus Sonus soundmap project.
 *	<http://locusonus.org/soundmap/>
 *
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	------------------------------------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LocusIce.h"


#define HOUR        3600
#define MINUTE      60
#define MILLISECOND 1000





// type of information on the audio stream
typedef NS_ENUM(NSInteger,infoTypes)
{
    INFO_TYPE_TIME,     // elapsed time
    INFO_TYPE_BYTES,    // bytes sent
    INFO_TYPE_PACKET,   // packet sent
    INFO_TYPE_END
};





@interface MainController : UIViewController <CLLocationManagerDelegate>
{
    CLLocationManager* locationManager;
    NSString* locationMessage;
    NSThread* infoLoopThread;
    NSInteger infoType;
    
    uint time;
}


@property (strong, nonatomic) IBOutlet UILabel  *message;
@property (strong, nonatomic) IBOutlet UILabel  *info;
@property (strong, nonatomic) IBOutlet UILabel  *type;
@property (strong, nonatomic) IBOutlet UIButton *remote;
@property (strong, nonatomic) IBOutlet UIButton *twitter;


- (void)activate;
- (void)configure;
- (void)sync;

- (IBAction)remoteAction:(id)sender;
- (void)remoteRefresh;
- (void)start;
- (void)stop;


- (void)infoInit;
- (void)infoStart;
- (void)infoStop;
- (void)infoUpdate;
- (void)infoClear;
- (void)infoTypeAction:(UITapGestureRecognizer *)tapGesture;


- (void)locationInit;
- (void)locationStart;
- (void)locationStop;


- (void)displayError:(NSString*) message;
- (void)displayMessage:(NSString*) message;
- (void)displayDefaultMessage;

- (IBAction)sendTweet:(id)sender;

@end
