/**
 * DataHolder.m
 * LocusCast project
 * This file is released under GNU General Public License, version 3 or later.
 */



#import "DataHolder.h"



NSString * const kLogName = @"kLogname";
NSString * const kLogPass = @"kLogpass";
NSString * const kAudioChannel = @"kAuchannel";
NSString * const kAudioSamplerate = @"kAusamplerate";
NSString * const kAudioQuality = @"kAuquality";
NSString * const kLocationTime = @"kLoctime";
NSString * const kLocationDistance = @"kLocdist";
NSString * const kSettingAutoReconnect = @"kSettingAutoReconnect";



@implementation DataHolder

- (id) init
{
    self = [super init];
    if (self)
    {
        // default value
        _logname = @"";
        _logpass = @"";
        _auchannel = 1;
        _ausamplerate = 44100;
        _auquality = 0.3f;
        _loctime = DEFAULT_LOC_TIMEOUT;
        _locdist = DEFAULT_LOC_DISTANCE;
        _autoreconnect = YES;
    }
    return self;
}

+ (DataHolder *)sharedInstance
{
    static DataHolder *_sharedInstance = nil;
    static dispatch_once_t onceSecurePredicate;
    dispatch_once(&onceSecurePredicate,^
    {
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - login
// -------------------------------------------------------------------------------------------------

- (void)loadLogin
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kLogName])
        _logname = [[NSUserDefaults standardUserDefaults] objectForKey:kLogName];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kLogPass])
        _logpass = [[NSUserDefaults standardUserDefaults] objectForKey:kLogPass];
}

- (void)saveLogin:(NSString*)name :(NSString*)pass
{
    _logname = name;
    _logpass = pass;
    
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:kLogName];
    [[NSUserDefaults standardUserDefaults] setObject:pass forKey:kLogPass];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isEmpty
{
    return [self.logname length]==0 && [self.logpass length]==0;
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - audio
// -------------------------------------------------------------------------------------------------

- (void)loadAudio
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAudioChannel])
        _auchannel = (int)[[NSUserDefaults standardUserDefaults] integerForKey:kAudioChannel];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAudioSamplerate])
        _ausamplerate = (int)[[NSUserDefaults standardUserDefaults] integerForKey:kAudioSamplerate];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kAudioQuality])
        _auquality = [[NSUserDefaults standardUserDefaults] floatForKey:kAudioQuality];
}

- (void)saveAudio:(int)channel :(float)quality :(int)samplerate
{
    _auchannel = channel;
    _auquality = quality;
    _ausamplerate = samplerate;
    
    [[NSUserDefaults standardUserDefaults] setInteger:channel forKey:kAudioChannel];
    [[NSUserDefaults standardUserDefaults] setInteger:samplerate forKey:kAudioSamplerate];
    [[NSUserDefaults standardUserDefaults] setFloat:quality forKey:kAudioQuality];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - location
// -------------------------------------------------------------------------------------------------

- (void) loadLocation
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kLocationTime])
        _loctime = (int)[[NSUserDefaults standardUserDefaults] integerForKey:kLocationTime];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kLocationDistance])
        _locdist = (int)[[NSUserDefaults standardUserDefaults] integerForKey:kLocationDistance];
}

- (void) saveLocation:(int)time :(int)distance
{
    _loctime = time;
    _locdist = distance;
    
    [[NSUserDefaults standardUserDefaults] setInteger:time forKey:kLocationTime];
    [[NSUserDefaults standardUserDefaults] setInteger:distance forKey:kLocationDistance];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - advanced settings
// -------------------------------------------------------------------------------------------------

- (void) loadAdvancedSettings
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kSettingAutoReconnect])
        _autoreconnect = [[NSUserDefaults standardUserDefaults] boolForKey:kSettingAutoReconnect];
}

- (void) saveAdvancedSettings:(BOOL)autoreconnect
{
    _autoreconnect = autoreconnect;
    
    [[NSUserDefaults standardUserDefaults] setBool:autoreconnect forKey:kSettingAutoReconnect];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end