/**
 * ConfigurationController.mm
 * LocusCast project
 * This file is released under GNU General Public License, version 3 or later.
 */



#import "LocusIce.h"
#import "DataHolder.h"
#import "ConfigurationController.h"



@implementation ConfigurationController




// -------------------------------------------------------------------------------------------------
    #pragma mark - ui
// -------------------------------------------------------------------------------------------------

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        
        // iPhone
        if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]) {
       
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(deviceOrientationDidChange:)
                                                         name:UIDeviceOrientationDidChangeNotification
                                                       object:nil];
        
        
            
            CGSize  screen   = [UIScreen mainScreen].bounds.size;
            UIView *inner    = self.view;
            UIView *overlay  = [[UIView alloc] initWithFrame:CGRectMake(0,0,screen.width,screen.height)];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
            
            
            [inner.layer setCornerRadius:7.0];
            [inner setFrame:CGRectMake( 0, 0, screen.width,screen.height )];
            
            [button addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"Close" forState:UIControlStateNormal];
            [button setFrame:CGRectMake( 0, 0, screen.width, 50 )];
            [button setBackgroundColor:inner.backgroundColor];
            [button.layer setCornerRadius:7.0];
            [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0]];

            [overlay setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.85]];
            [overlay addSubview:inner];
            [overlay addSubview:button];
            [overlay addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(tap:)]];

            
            
            [self setView:overlay];
            [self deviceOrientationDidChange:nil];
            [self.config setBackgroundColor:inner.backgroundColor];
        }
        
        // iPad
        else {
            [self.view setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.0]];
            [self.config setBackgroundColor:self.view.backgroundColor];
        }
    }
    return self;
}

- (void)deviceOrientationDidChange: (NSNotification *)notification
{
    
    // iPhone only
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]) {
        
        CGSize  screen = [UIScreen mainScreen].bounds.size;
        CGFloat width  = MIN( screen.width, screen.height ) - 16;
        CGFloat height = MAX( screen.height-136, width-58 );
        
        [self.view setFrame:CGRectMake( 0, 0, screen.width, screen.height )];
        [self.view.subviews[0] setFrame:CGRectMake( (screen.width/2)-(width/2), screen.height-height-66, width, height )];
        [self.view.subviews[1] setFrame:CGRectMake( (screen.width/2)-(width/2), screen.height-58, width, 50 )];
        
    }
}

- (void)tap:(UIGestureRecognizer *)gesture
{
    // iPhone only
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]) {
        UIView *inner = self.view.subviews[0];
        CGPoint point = [gesture locationInView:self.view];
        if ( !CGRectContainsPoint(inner.frame,point) ) [self close];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self refreshConfiguration];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - system
// -------------------------------------------------------------------------------------------------

- (void)refreshConfiguration
{
    [[DataHolder sharedInstance] loadAudio];
    
    setChannel( [DataHolder sharedInstance].auchannel );
    setQuality( [DataHolder sharedInstance].auquality );
    setSampleRate( [DataHolder sharedInstance].ausamplerate );
    
    NSString *appname = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *config  = [NSString stringWithUTF8String:getUserConfiguration().c_str()];
    NSString *appinfo = [NSString stringWithUTF8String:getAppInformation().c_str()];
    
    _appname.text = [[NSString stringWithFormat:@"%@", appname] uppercaseString];
    _version.text = [NSString stringWithFormat:@"version %@", version];
    _config.text  = [NSString stringWithFormat:@"active network type:\n%@\n\n%@\n\n%@", [self newtworkType], config, appinfo];
    
    [_config scrollRangeToVisible:NSMakeRange(0,0)];
}

- (NSString*)newtworkType
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus netstatus = [reachability currentReachabilityStatus];
    
    
    if ( netstatus == ReachableViaWiFi ) return @"Wi-Fi";
    if ( netstatus == ReachableViaWWAN ) {
        
        CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
        NSString *radio = netinfo.currentRadioAccessTechnology;
        
        // 4G
        if( [radio isEqualToString:CTRadioAccessTechnologyLTE] )                return @"4G";
        // 3G
        else if( [radio isEqualToString:CTRadioAccessTechnologyWCDMA] )         return @"3G";
        else if( [radio isEqualToString:CTRadioAccessTechnologyHSDPA] )         return @"3G";
        else if( [radio isEqualToString:CTRadioAccessTechnologyHSUPA] )         return @"3G";
        else if( [radio isEqualToString:CTRadioAccessTechnologyCDMAEVDORev0] )  return @"3G";
        else if( [radio isEqualToString:CTRadioAccessTechnologyCDMAEVDORevA] )  return @"3G";
        else if( [radio isEqualToString:CTRadioAccessTechnologyCDMAEVDORevB] )  return @"3G";
        else if( [radio isEqualToString:CTRadioAccessTechnologyeHRPD] )         return @"3G";
        // 2G
        else if( [radio isEqualToString:CTRadioAccessTechnologyGPRS] )          return @"GPRS";
        else if( [radio isEqualToString:CTRadioAccessTechnologyEdge] )          return @"EDGE";
        else if( [radio isEqualToString:CTRadioAccessTechnologyCDMA1x] )        return @"2G";
    }
    
    // if ( netstatus == NotReachable )
    return @"No wifi or cellular connection";
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - event
// -------------------------------------------------------------------------------------------------

- (IBAction)copy
{

    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [NSString stringWithFormat:@"%@:\n%@\n\n%@", _appname.text, _version.text, _config.text ];
    
    [self close];
}

- (IBAction)close
{
    // iPhone
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]) {
        [UIView animateWithDuration:0.3f
                              animations:^{
                                  self.view.alpha = 0.0f;
                              }
                              completion:^(BOOL finished)
                              {
                                  [self.view removeFromSuperview];
                                  [self removeFromParentViewController];
                              }
              ];
    }
    
    // iPad
    else [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}

@end
