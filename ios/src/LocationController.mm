/**
 * LocationController.mm
 * LocusCast project
 * This file is released under GNU General Public License, version 3 or later.
 */



#import "LocusIce.h"
#import "DataHolder.h"
#import "LocationController.h"
//#import <CoreLocation/CoreLocation.h>



@interface LocationController ()
{
    int time;
    int distance;
}
@end


@implementation LocationController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[DataHolder sharedInstance] loadLocation];
    
    [self.timeSlider setValue:[DataHolder sharedInstance].loctime animated:FALSE];
    [self setTime:_timeSlider];
    
    [self.distanceSlider setValue:[DataHolder sharedInstance].locdist animated:FALSE];
    [self setDistance:_distanceSlider];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [[DataHolder sharedInstance] saveLocation:time:distance];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - update
// -------------------------------------------------------------------------------------------------

- (IBAction)setTime:(UISlider*)control
{
    time = (int)control.value;
    _timeLabel.text = [NSString stringWithFormat:@"%i", time];
    
    UILabel *footer = [[self.tableView footerViewForSection:1] textLabel];
    if ( time/60==0 ) footer.text = [NSString stringWithFormat:@"%.2d seconds", time%60];
    else footer.text = [NSString stringWithFormat:@"%.2d minute %.2d seconds", time/60, time%60];
    
    footer.frame = CGRectMake( footer.frame.origin.x,
                               footer.frame.origin.y,
                               self.tableView.frame.size.width - footer.frame.origin.x*2,
                               footer.frame.size.height );
}

- (IBAction)setDistance:(UISlider*)control
{
    distance = (int)control.value;
    _distanceLabel.text = [NSString stringWithFormat:@"%i", distance];
    
    UILabel *footer = [[self.tableView footerViewForSection:0] textLabel];
    if ( distance==0 ) footer.text = @"all movements";
    else if ( distance/1000==0 ) footer.text = [NSString stringWithFormat:@"%d meters", distance];
    else footer.text = [NSString stringWithFormat:@"%d kilometers", distance/1000];
    
    footer.frame = CGRectMake( footer.frame.origin.x,
                              footer.frame.origin.y,
                              self.tableView.frame.size.width - footer.frame.origin.x*2,
                              footer.frame.size.height );
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - ui
// -------------------------------------------------------------------------------------------------

//- (void)viewDidAppear:(BOOL)animated
//{
//    if ( ![CLLocationManager deferredLocationUpdatesAvailable] ) {
//        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:NO];
//    }
//}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return [CLLocationManager deferredLocationUpdatesAvailable]+1;
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
//{
//    
//    BOOL candefer = [CLLocationManager deferredLocationUpdatesAvailable];
//    
//    NSString *cur = [[[tableView footerViewForSection:section] textLabel] text];
//    NSString *end = @"Changes take effect when starting or restarting your stream.";
//    
//    switch (section) {
//        case 0:
//            if ( [cur length]==0 ) cur = @"in meters (set 0 to specify all movements)";
//            return candefer ? cur : [NSString stringWithFormat:@"%@\n\n%@\n\n%@", cur, @"Continued use of GPS can dramatically decrease battery life. Prefers defer the delivery of location updates by setting the proper minimum distance you need.", end];
//        case 1:
//            if ( [cur length]==0 ) cur = @"in seconds";
//            return !candefer ? cur : [NSString stringWithFormat:@"%@\n\n%@\n\n%@", cur, @"Continued use of GPS running in the background can dramatically decrease battery life. Prefers defer the delivery of location updates by setting the proper minimum distance and timeout you need. When one or both criteria are met, the location manager delivers the cached locations.\nIf the app is running in the foreground, the location manager does not defer the deliver of events and use the specified distance criteria only.", end];
//        default:
//            return [[[tableView footerViewForSection:section] textLabel] text];
//    }
//}
//
- (IBAction)refreshFooterTitles
{
    [self.tableView reloadData];
}

@end
