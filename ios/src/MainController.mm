/**
 * MainController.mm
 * LocusCast project
 * This file is released under GNU General Public License, version 3 or later.
 */



#import <Social/Social.h>

#import "DataHolder.h"
#import "MainController.h"
#import "Reachability.h"


// C++ implementation
const char* getAppName()
{
    NSString *appname = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    return [appname UTF8String];
}

const char* getAppVersion()
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    return [version UTF8String];
}









@interface MainController ()

- (void)run;
- (void)infoLoop;
- (BOOL)isDarkMode;


@property (nonatomic) Reachability *reachability;
- (void)networkChange:(NSNotification *)notification;

@end

@implementation MainController



BOOL isRunning     = false;
BOOL isConfigured  = false;
BOOL isInited      = false;



// -------------------------------------------------------------------------------------------------
    #pragma mark - setup
// -------------------------------------------------------------------------------------------------

- (void)activate
{
    [self infoClear];
    [self remoteRefresh];
    
    if ( isInited ) return;
    
    // init stream information manager
    [self infoInit];
    
    // open setting page if login/pass are missing
    [[DataHolder sharedInstance] loadLogin];
    if ( [[DataHolder sharedInstance] isEmpty] )[self performSegueWithIdentifier:@"openSetupView" sender:self];
    
    
    // handle network change
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChange:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    self.reachability = [Reachability reachabilityWithHostName:@SERVER_HOST];//[Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    
    isInited = true;
}

- (void)configure
{
    
    [[DataHolder sharedInstance] loadLogin];
    [[DataHolder sharedInstance] loadAudio];
    
    // nothing to do without proper login/pass
    if ( [[DataHolder sharedInstance] isEmpty] ) return;

    
    // make stream configuration
    isConfigured = autoconfig([[DataHolder sharedInstance].logname UTF8String],
                              [[DataHolder sharedInstance].logpass UTF8String]);
    
        
    // override audio values by user preferences
    setChannel( [DataHolder sharedInstance].auchannel );
    setQuality( [DataHolder sharedInstance].auquality );
    setSampleRate( [DataHolder sharedInstance].ausamplerate );
    
    
    // update interface
    if ( !isConfigured ) [self displayMessage:[NSString stringWithUTF8String:getLastError()]];
    else [self displayDefaultMessage];
    [self remoteRefresh];
    
}

- (void)sync
{
    // sync configuration with server
    [[DataHolder sharedInstance] loadLogin];
    if ( ![[DataHolder sharedInstance] isEmpty] ) {
        [self displayMessage:@"Fetching stream configuration …"];
        NSThread* thread = [[NSThread alloc] initWithTarget:self selector:@selector(configure) object:nil ];
        [thread start];
    }
}



// -------------------------------------------------------------------------------------------------
    #pragma mark - ui
// -------------------------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self infoClear];
    [self remoteRefresh];
    
    // add location support if user is registered
    [[DataHolder sharedInstance] loadLogin];
    if ( ![[DataHolder sharedInstance] isEmpty] ) [self locationInit];
    
    
    
    // iPad only
    if ( [[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad ) {
        
        [self.twitter setTitle:@"send tweets" forState:UIControlStateNormal];
        [self.twitter setTitleColor:self.remote.titleLabel.textColor forState:UIControlStateNormal];
        
        [self.info setFont:[self.info.font fontWithSize:120]];
        [self.info.superview.constraints[1] setConstant:640];
        [self.info.superview.constraints[0] setConstant:140];
    }
    
    // load server configuration in asynchronous manner
    [self sync];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self stop];
}

// fix upside down orientation
- (BOOL)shouldAutorotate { return YES; }
- (NSUInteger)supportedInterfaceOrientations { return UIInterfaceOrientationMaskAll; }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [self stop];
    if ( isRunning ) [self displayError:@"YOUR STREAM IS STOPPED\nApplication receives a memory warning."];
}

- (void)displayError:(NSString *)message
{
    NSString *appname = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"] uppercaseString];
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue]<8.0 ) {
        [[[UIAlertView alloc] initWithTitle:appname
                                    message:message
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil]
         show];
    }
    else {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:appname message:message preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                //[self displayMessage:(isConfigured ? [NSString stringWithUTF8String:getUrl()] : @"")];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }

}

- (void)displayMessage:(NSString *)message
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.message.text = message;
    }];
}

- (void)displayDefaultMessage
{
    [self displayMessage:[[NSString stringWithUTF8String:getName()] uppercaseString]];
}

- (BOOL)isDarkMode
{
    if (@available(iOS 13.0, *)) {
        if ( UITraitCollection.currentTraitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ) return YES;
    }
    
    return NO;
}



// -------------------------------------------------------------------------------------------------
    #pragma mark - control
// -------------------------------------------------------------------------------------------------

- (void)remoteRefresh
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.remote setTitle:(isRunning ? @"STOP" : @"START") forState:UIControlStateNormal];
        [self.remote setEnabled:isConfigured];
        
        if ( isConfigured ) {
            [self.remote.layer setCornerRadius:3.0];
            [self.remote.layer setBorderWidth:1.0];
            [self.remote.layer setBorderColor:[[UIColor colorWithWhite:(self.isDarkMode ? 0.08 : 0.92) alpha:1] CGColor]];
        }
    });
}

- (IBAction)remoteAction:(id)sender
{
    if ( !isRunning ) [self start];
    else [self stop];
}

- (void)start
{
    if ( isRunning ) return;
    
    [self.remote setEnabled:NO];
    [self displayMessage:(streamInit() ? @"Initialize audio encoder" : [NSString stringWithUTF8String:getLastError()])];
    
    NSThread* thread = [[NSThread alloc] initWithTarget:self selector:@selector(run) object:nil ];
    [thread start];
    
}

// wait for audio encoder initialization
// must be call in thread
- (void)run
{
    isRunning = streamStart();
    if ( isRunning ) {
        [self locationStart];
        [self infoStart];
    }
    else [self displayError:[NSString stringWithUTF8String:getLastError()]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self remoteRefresh];
        [self.remote setEnabled:YES];
    });
}

- (void)stop
{
    if ( !isRunning ) return;
    
    isRunning = false;
    
    [self infoStop];
    [self locationStop];
    [self remoteRefresh];
    
    streamStop();
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - stream information
// -------------------------------------------------------------------------------------------------

- (void)infoInit
{
    if ( !isInited ) infoType = INFO_TYPE_TIME;
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.info setFont:[UIFont monospacedDigitSystemFontOfSize:[[self.info font] pointSize]  weight:UIFontWeightBold]];
        [self.info setUserInteractionEnabled:YES];
        [self.info addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoTypeAction:)]];
        [self infoClear];
    }];
}

- (void)infoStart
{
    [self infoInit]; // reallocate gesture recognizer
    
    infoLoopThread = [[NSThread alloc] initWithTarget:self selector:@selector(infoLoop) object:nil ];
    [infoLoopThread start];
}

- (void)infoStop
{
    [infoLoopThread cancel];
    infoLoopThread = nil;
}

- (void)infoUpdate
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        long bytes;
        
        // streaming informations
        switch( infoType )
        {
            case INFO_TYPE_BYTES  :
                bytes = streamGetTransferedBytes();
                self.info.text = [NSByteCountFormatter stringFromByteCount:bytes countStyle:NSByteCountFormatterCountStyleBinary];
                self.type.text = [NSString stringWithFormat:@"%@ bytes", [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithInteger:bytes] numberStyle:NSNumberFormatterDecimalStyle]];
                break;
                
            case INFO_TYPE_PACKET :
                self.info.text = [NSString stringWithFormat:@"%i", streamGetTransferedPackets()];
                self.type.text = @"packets";
                break;
                
            default:
                self.info.text = [NSString stringWithFormat:@"%02d:%02d:%02d", (time/HOUR)%24, (time/MINUTE)%60, time%60];
                self.type.text = @"";
                break;
        }
        
        
    }];
}

- (void)infoLoop
{
    [[DataHolder sharedInstance] loadAdvancedSettings];
    
    BOOL reconnect = [DataHolder sharedInstance].autoreconnect;
    
    NSThread * thread = [NSThread currentThread];
    NSArray *messages = [NSArray arrayWithObjects:
                         @"",
                         [NSString stringWithFormat:@"stream name\n%@", [NSString stringWithUTF8String:getName()]],
                         [NSString stringWithFormat:@"stream url\n%@",  [NSString stringWithUTF8String:getUrl()]],
                         [NSString stringWithFormat:@"mountpoint\n/%@", [NSString stringWithUTF8String:getMountpoint()]],
                         nil];
    
    time = 0;
    int cycle = 0;
    while( !thread.isCancelled && isRunning ) {
        
        [self infoUpdate];
        sleep(1);
        time += 1;
        
        
        
        // check for broken connection
        // or encoding error
        if ( time%3==0 ) {
            
            if ( isRunning && !streamIsMounted() ) {
                if ( reconnect ) {
                    [self displayMessage:@"Broken connection\nreconnecting …"];
                    streamStop();
                    streamStart();
                    continue;
                }
                else {
                    [self displayError:@"Broken connection"];
                    [self stop];
                    break;
                }
            }
            
            if ( isRunning && !streamIsEncoding() ) {
                NSString *err = [NSString stringWithUTF8String:getLastError()];
                [self displayError: [err length]==0 ? @"Audio encoder stopped unexpectedly" : err];
                [self stop];
                break;
            }
        }
        
        
        // stream informations cycle
        if ( time%7==0 ) {
            if ( ++cycle==[messages count] ) cycle = 0;
            [self displayMessage:cycle ? messages[cycle] : locationMessage];
        }
    }
    
    [self displayDefaultMessage];
    [self infoClear];
}

- (void)infoClear
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.info.text = @"";
        self.type.text = @"";
    });
}

- (void)infoTypeAction:(UITapGestureRecognizer *)tapGesture
{
    if ( isRunning ) {
        (++infoType) %=INFO_TYPE_END;
        [self infoUpdate];
    }
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - location
// -------------------------------------------------------------------------------------------------

- (void)locationInit
{
    if ( [CLLocationManager locationServicesEnabled] )
	{
        locationManager = [[CLLocationManager alloc] init];
        locationMessage = @"waiting for location service…";
        
        locationManager.delegate = self;
        locationManager.activityType = CLActivityTypeOther;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.pausesLocationUpdatesAutomatically = NO;
        
        if ( [[[UIDevice currentDevice] systemVersion] floatValue]>=8.0 ) [locationManager requestAlwaysAuthorization];
    }
    else [self displayError:@"LOCATION SERVICE IS DISABLED\nSee the Privacy Settings for more informations."];
}

- (void)locationStart
{
    if ( [CLLocationManager locationServicesEnabled] ) {
        [[DataHolder sharedInstance] loadLocation];
        [locationManager setDistanceFilter:[DataHolder sharedInstance].locdist ?: kCLDistanceFilterNone];
        [locationManager setDistanceFilter:kCLDistanceFilterNone];
        [locationManager startUpdatingLocation];
    }
}

- (void)locationStop
{
    if ( [CLLocationManager locationServicesEnabled] ) {
        [locationManager disallowDeferredLocationUpdates];
        [locationManager stopUpdatingLocation];
        [self displayDefaultMessage];
    }
}

NSInteger locationError = 0; // to display error once
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    switch([error code])
    {
        case kCLErrorNetwork:
            if ( locationError==kCLErrorNetwork ) [self displayMessage:@"Failed to get your location"];
            else [self displayError:@"FAILED TO GET YOUR LOCATION\nPlease check your network connection or that you are not in airplane mode"];
            break;
        case kCLErrorDenied:
            if ( locationError!=kCLErrorDenied ) [self displayError:@"LOCATION IS NOT ALLOWED\nCheck your Privacy Settings."];
            else [self displayMessage:@"Location service is not allowed by user"];
            break;
        default:
            [self displayMessage:@"Failed to get your location"];
            break;
    }
    locationError = error.code;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSString *str;
    CLLocation *currentLocation = [locations lastObject];
    
    if ( locations!=nil ) {
        double lat = currentLocation.coordinate.latitude;
        double lng = currentLocation.coordinate.longitude;
        
        if ( sendLocation(lat,lng) ) str = [NSString stringWithFormat:@"latitude: %g - longitude: %g",lat,lng];
        else str = [NSString stringWithFormat:@"Failed to save your location\n%@",[NSString stringWithUTF8String:getLastError()]];
        
        // display message on change
        if ( ![locationMessage isEqual:str] ) {
            [self displayMessage:str];
            locationMessage = str;
        }
    }
    
    
    if ( [CLLocationManager deferredLocationUpdatesAvailable] ) {
        [locationManager setDistanceFilter:kCLDistanceFilterNone];
    }
    
    [[DataHolder sharedInstance] loadLocation];
    [locationManager allowDeferredLocationUpdatesUntilTraveled:[DataHolder sharedInstance].locdist
                                                       timeout:[DataHolder sharedInstance].loctime*MILLISECOND];
    
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - network
// -------------------------------------------------------------------------------------------------


// FIXME:
- (void)networkChange:(NSNotification *)notification
{
    Reachability* reachability = [notification object];
    if ( reachability.currentReachabilityStatus!=NotReachable && !isConfigured ) [self sync];
}

- (IBAction)sendTweet:(id)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweet setInitialText:@"#locustream "];
        [self presentViewController:tweet animated:YES completion:nil];
    }
    else [self displayError:@"You can't send a tweet right now, make sure your device has an internet connection, Twitter app installed and you have at least one Twitter account setup"];
}

@end
