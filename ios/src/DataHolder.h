/**
 *	DataHolder.h
 *	Stéphane Cousot @ http://www.ubaa.net/
 *	LocusCast is released under GNU General Public License, version 3 or later
 *
 *
 *	This file is part of the LocusCast application.
 *	LocusCast is a mobile audio streaming app for Locus Sonus soundmap project.
 *	<http://locusonus.org/soundmap/>
 *
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	------------------------------------------------------------------------------------------------
 */

#import <Foundation/Foundation.h>


#define DEFAULT_LOC_TIMEOUT     10     // The amount of time between location updates, in seconds
#define DEFAULT_LOC_DISTANCE    100    // minimum distance between location updates, in meters




@interface DataHolder : NSObject

+ (DataHolder*)sharedInstance;

@property (assign) NSString* logname;
@property (assign) NSString* logpass;

- (void) loadLogin;
- (void) saveLogin:(NSString*)name :(NSString*)pass;
- (BOOL) isEmpty;

@property (assign) int auchannel;
@property (assign) int ausamplerate;
@property (assign) float auquality;

- (void) loadAudio;
- (void) saveAudio:(int)channel :(float)quality :(int)samplerate;

@property (assign) int loctime;
@property (assign) int locdist;

- (void) loadLocation;
- (void) saveLocation:(int)time :(int)distance;

@property (assign) BOOL autoreconnect;

- (void) loadAdvancedSettings;
- (void) saveAdvancedSettings:(BOOL)autoreconnect;

@end
