/**
 * AudioController.mm
 * LocusCast project
 * This file is released under GNU General Public License, version 3 or later.
 */



#import "LocusIce.h"
#import "DataHolder.h"
#import "AudioController.h"



@interface AudioController ()
{
    int channel;
    int samplerate;
    float quality;
    
    BOOL editSampleRate;
}

@end


@implementation AudioController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // hide samplerate cell/picker by defaut
    editSampleRate = FALSE;
    
    [[DataHolder sharedInstance] loadAudio];
    
    [self.channelSwitch setOn:[DataHolder sharedInstance].auchannel-1];
    [self.qualitySlider setValue:[DataHolder sharedInstance].auquality animated:FALSE];
    [self.sampleRateButton setTitle:[NSString stringWithFormat:@"%d Hz",samplerate] forState:UIControlStateNormal];
    
    [self setChannel:self.channelSwitch];
    [self setQuality:self.qualitySlider];
    [self setSampleRate:[DataHolder sharedInstance].ausamplerate];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [[DataHolder sharedInstance] saveAudio:channel:quality:samplerate];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - update
// -------------------------------------------------------------------------------------------------

- (IBAction)setChannel:(UISwitch*)control
{
    channel = [self.channelSwitch isOn]+1;
}

- (IBAction)setQuality:(UISlider*)control
{
    quality = [[NSString stringWithFormat:@"%.1f",control.value]floatValue];
    [self.qualityLabel setText:[NSString stringWithFormat:@"%.1f", quality]];
}

- (void)setSampleRate:(int)value
{
    samplerate = value;
    [self.sampleRateButton setTitle:[NSString stringWithFormat:@"%d Hz", samplerate] forState:UIControlStateNormal];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - picker
// -------------------------------------------------------------------------------------------------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return SAMPLE_RATES.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@ Hz", SAMPLE_RATES[row]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self setSampleRate:[(NSNumber*)[SAMPLE_RATES objectAtIndex:row] integerValue]];
    [self closeSampleRateCell];
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - cell
// -------------------------------------------------------------------------------------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // picker cell
    if ( indexPath.row==2 ) {
        [self.sampleRatePicker setHidden:!editSampleRate];
        return editSampleRate * 162;
    }
    
    // quality cell
    if ( indexPath.row==3 ) return 188;
    
    return self.tableView.rowHeight;
}

- (IBAction)toggleSampleRateCell
{
    editSampleRate = !editSampleRate;
    [self.tableView reloadData];
    
    if ( editSampleRate ) {
        for( int i=0; i<SAMPLE_RATES.count; i++ ) {
            if ( [(NSNumber*)SAMPLE_RATES[i] integerValue]==samplerate ) {
                [self.sampleRatePicker selectRow:i inComponent:0 animated:TRUE];
                break;
            }
        }
    }
}

- (IBAction)closeSampleRateCell
{
    editSampleRate = FALSE;
    [self.tableView reloadData];
}

@end
