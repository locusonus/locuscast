/**
 * SetupController.mm
 * LocusCast project
 * This file is released under GNU General Public License, version 3 or later.
 */



#import "LocusIce.h"
#import "DataHolder.h"
#import "SetupController.h"
#import "ConfigurationController.h"



@implementation SetupController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[DataHolder sharedInstance] loadLogin];
    [[DataHolder sharedInstance] loadAdvancedSettings];
    
    [self.loginName setText:[DataHolder sharedInstance].logname];
    [self.loginPass setText:[DataHolder sharedInstance].logpass];
    [self.arSwitch setOn:[DataHolder sharedInstance].autoreconnect];
    
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - ui
// -------------------------------------------------------------------------------------------------

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    NSString *name;
    switch (section) {
        case 0:
            name = [NSString stringWithUTF8String:getName()];
            return [name length]==0 ? @"" :
            [NSString stringWithFormat:@"Currently configured for the stream name :\n%@", name];
        case 1 :
            return @"";
        case 2 :
            return @"Changes take effect when starting or restarting your stream.";
        default:
            return [[[tableView footerViewForSection:section] textLabel] text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if ( textField==_loginName ) [self.loginPass becomeFirstResponder];
    else if ( textField==self.loginPass ) {
        [self loginDown];
        [self loginUp];
    }
    else [textField resignFirstResponder];
    
    return NO;
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - login
// -------------------------------------------------------------------------------------------------

- (IBAction)loginDown
{
    [self.loginActivity setHidden:FALSE];
}

- (IBAction)loginUp
{
    
    NSString *name = [_loginName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *pass = [_loginPass.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ( [name length]==0 || [pass length]==0 ) {
        [self.loginActivity setHidden:TRUE];
        return;
    }
    
    BOOL logged = autoconfig( [name UTF8String], [pass UTF8String] );
    if ( logged ) {
        [[DataHolder sharedInstance] saveLogin:name:pass];
        [[UIApplication sharedApplication] sendAction:_backButton.action
                                                   to:_backButton.target
                                                 from:nil
                                             forEvent:nil];
    }
    else [self loginError];
    
    [self.loginActivity setHidden:TRUE];
}

- (void)loginError
{
    NSString *appname = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"] uppercaseString];
    NSString *message = [NSString stringWithUTF8String:getLastError()];
    
    if ( [[[UIDevice currentDevice] systemVersion] floatValue]<8.0 ) {
        [[[UIAlertView alloc] initWithTitle:appname
                                    message:message
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil]
         show];
    }
    else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:appname
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}




// -------------------------------------------------------------------------------------------------
    #pragma mark - advanced settings
// -------------------------------------------------------------------------------------------------

- (void)setAutoReconnect:(UISwitch *)control
{
    [[DataHolder sharedInstance] saveAdvancedSettings:[control isOn]];
}


// -------------------------------------------------------------------------------------------------
    #pragma mark - configuration
// -------------------------------------------------------------------------------------------------

- (IBAction)popoverConfiguration:(id)sender
{

    ConfigurationController *config = [[ConfigurationController alloc] initWithNibName:@"Configuration" bundle:nil];
    
    // iPhone
    if ([[UIDevice currentDevice].model hasPrefix:@"iPhone"]) {
        UINavigationController *nav = self.navigationController;
        [nav addChildViewController:config];
        [nav.view addSubview:config.view];
        [nav.view bringSubviewToFront:config.view];
        [nav popToViewController:config animated:YES];
    }
    
    // iPad
    else {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:config];
        popover.popoverContentSize = CGSizeMake(370.0,600.0);
        [popover presentPopoverFromRect:CGRectMake(140.0,40.0,0.0,0.0)
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
}

@end
