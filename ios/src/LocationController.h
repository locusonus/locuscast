/**
 *	LocationController.h
 *	Stéphane Cousot @ http://www.ubaa.net/
 *	LocusCast is released under GNU General Public License, version 3 or later
 *
 *
 *	This file is part of the LocusCast application.
 *	LocusCast is a mobile audio streaming app for Locus Sonus soundmap project.
 *	<http://locusonus.org/soundmap/>
 *
 *
 *	------------------------------------------------------------------------------------------------
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	------------------------------------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>




@interface LocationController : UITableViewController

@property (strong, nonatomic) IBOutlet UILabel  *timeLabel;
@property (strong, nonatomic) IBOutlet UISlider *timeSlider;
@property (strong, nonatomic) IBOutlet UILabel  *distanceLabel;
@property (strong, nonatomic) IBOutlet UISlider *distanceSlider;

- (IBAction)setTime:(UISlider*)control;
- (IBAction)setDistance:(UISlider*)control;
- (IBAction)refreshFooterTitles;

@end
